<?php

declare (strict_types=1);
namespace Xtwoend\HySubscribe\Model;

use Carbon\Carbon;
use LogicException;
use BadMethodCallException;
use Hyperf\DbConnection\Db;
use UnexpectedValueException;
use Xtwoend\HySubscribe\Period;
use Hyperf\Database\Model\Builder;
use Hyperf\DbConnection\Model\Model;
use Xtwoend\HySubscribe\Trait\HasSchedules;
use Hyperf\Database\Model\Relations\BelongsTo;
use Xtwoend\HySubscribe\Trait\HasGracePeriodUsage;
use Xtwoend\HySubscribe\Trait\HasTrialPeriodUsage;
use Xtwoend\HySubscribe\Trait\HasSubscriptionPeriodUsage;

/**
 */
class PlanSubscription extends Model
{
    use HasSubscriptionPeriodUsage;
    use HasGracePeriodUsage;
    use HasTrialPeriodUsage;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'plan_subscriptions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tag',
        'user_id',
        'plan_id',
        'name',
        'description',
        'price',
        'trial_period',
        'trial_interval',
        'grace_period',
        'grace_interval',
        'invoice_period',
        'invoice_interval',
        'payment_method',
        'tier',
        'trial_ends_at',
        'starts_at',
        'ends_at',
        'cancels_at',
        'canceled_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'tag' => 'string',
        'price' => 'float',
        'trial_period' => 'integer',
        'trial_interval' => 'string',
        'grace_period' => 'integer',
        'grace_interval' => 'string',
        'invoice_period' => 'integer',
        'invoice_interval' => 'string',
        'payment_method' => 'string',
        'tier' => 'integer',
        'trial_ends_at' => 'datetime',
        'starts_at' => 'datetime',
        'ends_at' => 'datetime',
        'cancels_at' => 'datetime',
        'canceled_at' => 'datetime'
    ];

    /**
     * relation belongs_to plan
     */
    public function plan(): BelongsTo
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    /**
     * Synchronize subscription data with plan
     * @param Plan|null $plan
     * @param bool $syncInvoicing Synchronize billing frequency or leave it unchanged
     * @return PlanSubscription
     */
    public function syncPlan(Plan $plan = null, bool $syncInvoicing = true): PlanSubscription
    {
        $this->plan_id = $plan->id;
        $this->price = $plan->price;
        $this->tier = $plan->tier;
        $this->grace_interval = $plan->grace_interval;
        $this->grace_period = $plan->grace_period;

        if ($syncInvoicing) {
            // Set same invoicing as selected plan
            $this->invoice_interval = $plan->invoice_interval;
            $this->invoice_period = $plan->invoice_period;
        }

        $this->save();

        return $this;
    }

    /**
     * Change subscription plan.
     *
     * @param Plan|PlanCombination $planCombination Plan or PlanCombination model instance of the desired change
     * @param bool $clearUsage Clear subscription usage
     * @param bool $syncInvoicing Synchronize billing frequency or leave it unchanged
     * @return $this
     * @throws \Exception
     */
    public function changePlan(Plan $plan, bool $syncInvoicing = true): PlanSubscription
    {
        // Synchronize subscription data with plan
        $this->syncPlan($plan, $syncInvoicing, true);

        return $this;
    }

    /**
     * Renew subscription period.
     *
     * @param int $periods Number of periods to renew
     * @return $this
     * @throws \Exception
     */
    public function renew(int $periods = 1): PlanSubscription
    {
        if ($this->isCanceled()) {
            throw new LogicException('Unable to renew canceled subscription.');
        }

        Db::transaction(function () use ($periods) {
            // End trial
            if ($this->isOnTrial()) {
                $this->trial_ends_at = Carbon::now();
            }

            $isNew = !$this->starts_at; // Has never started subscription, so is new

            if ($isNew) {
                $period = new Period($this->invoice_interval, $this->invoice_period * $periods, Carbon::now());

                // If is new, period will need to have start and end date
                $this->starts_at = $period->getStartDate();
                $this->ends_at = $period->getEndDate();

                // Adjust ending dates depending on trial modes
                if ($this->plan->trial_mode === 'inside') {
                    // If trial time is considered time of subscription
                    // we renew subscription and substract from period used days
                    $this->ends_at->subDays($this->getTrialPeriodUsageIn('day'));
                } else if ($this->plan->trial_mode === 'outside') {
                    // Don't penalize early buyers
                    $this->ends_at->addDays($this->getTrialPeriodRemainingUsageIn('day'));
                }
            } else {
                // If it's not a new subscription, there are two options about renewal
                // 1. Period was ended sometime ago and did not renew: Set again start and end date, so there
                // is no confusion about if the user had subscription active during the months that was inactive
                // 2. Period is ongoing: Set end date to calculated end period
                $startDate = $this->hasEnded() ? Carbon::now() : $this->ends_at;
                $period = new Period($this->invoice_interval, $this->invoice_period * $periods, $startDate);

                if ($this->hasEnded()) $this->starts_at = $period->getStartDate();
                $this->ends_at = $period->getEndDate();
            }

            $this->save();
        });

        return $this;
    }

    /**
     * Cancel subscription.
     * When a fallback plan is set in config, subscription will never be cancelled but changed to that plan.
     * @param bool $immediately
     * @param bool $ignoreFallback
     * @return $this
     * @throws \Exception
     */
    public function cancel(bool $immediately = false, bool $ignoreFallback = false): PlanSubscription
    {
        if (!$ignoreFallback && config('subscribe.fallback_plan_tag')) { // Do not cancel if a fallback plan is set
            $plan = Plan::getByTag(config('subscribe.fallback_plan_tag'));
            if (!$plan) {
                throw new UnexpectedValueException('Fallback plan ' . config('subscribe.fallback_plan_tag') . ' does not exist.');
            }
            $this->changePlan($plan);
        } else {
            $this->canceled_at = Carbon::now();

            // If cancel is immediate, set end date
            if ($immediately) {
                // Cancel trial
                if ($this->isOnTrial()) $this->trial_ends_at = $this->canceled_at;

                // Cancel subscription
                $this->cancels_at = $this->canceled_at;
                $this->ends_at = $this->canceled_at;
            } else {
                // If cancel is not immediate, it will be cancelled at trial or period end
                $this->cancels_at = ($this->isOnTrial()) ? $this->trial_ends_at : $this->ends_at;
            }

            $this->save();
        }

        return $this;
    }

    /**
     * Uncancel subscription
     *
     * This action undoes all cancel flags
     *
     * @return $this
     */
    public function uncancel(): PlanSubscription
    {
        $this->canceled_at = null;
        $this->cancels_at = null;

        $this->save();

        return $this;
    }

    /**
     * Scope subscriptions with ending trial.
     *
     * @param \Hyperf\Database\Model\Builder $builder
     * @param int $dayRange
     *
     * @return \Hyperf\Database\Model\Builder
     */
    public function scopeFindEndingTrial(Builder $builder, int $dayRange = 3): Builder
    {
        $from = Carbon::now();
        $to = Carbon::now()->addDays($dayRange);

        return $builder->whereBetween('trial_ends_at', [$from, $to]);
    }

    /**
     * Scope subscriptions with ended trial.
     *
     * @param \Hyperf\Database\Model\Builder $builder
     *
     * @return \Hyperf\Database\Model\Builder
     */
    public function scopeFindEndedTrial(Builder $builder): Builder
    {
        return $builder->where('trial_ends_at', '<=', Carbon::now());
    }

    /**
     * Scope subscriptions with ending periods.
     *
     * @param \Hyperf\Database\Model\Builder $builder
     * @param int $dayRange
     *
     * @return \Hyperf\Database\Model\Builder
     */
    public function scopeFindEndingPeriod(Builder $builder, int $dayRange = 3): Builder
    {
        $from = Carbon::now();
        $to = Carbon::now()->addDays($dayRange);

        return $builder->whereBetween('ends_at', [$from, $to]);
    }

    /**
     * Scope subscriptions with ended periods.
     *
     * @param \Hyperf\Database\Model\Builder $builder
     *
     * @return \Hyperf\Database\Model\Builder
     */
    public function scopeFindEndedPeriod(Builder $builder): Builder
    {
        return $builder->where('ends_at', '<=', Carbon::now());
    }

    /**
     * Scope subscriptions that are payment pending
     *
     * @param \Hyperf\Database\Model\Builder $builder
     * @param Carbon|null $date Moment in time when to check
     * @return \Hyperf\Database\Model\Builder
     */
    public function scopeFindPendingPayment(Builder $builder, ?Carbon $date = null): Builder
    {
        if (!$date) {
            $date = Carbon::now();
        }

        return $builder->where(function (Builder $query) use ($date) {
            return $query->whereNull('canceled_at')
                ->orWhere('canceled_at', '>', $date);
        })
            ->where(function (Builder $query) use ($date) {
                return $query->whereNull('ends_at')
                    ->orWhere('ends_at', '<', $date);
            });
    }

    /**
     * Scope subscriptions by tag.
     *
     * @param \Hyperf\Database\Model\Builder $builder
     * @param string $tag
     *
     * @return \Hyperf\Database\Model\Builder
     */
    public function scopeGetByTag(Builder $builder, string $tag): Builder
    {
        return $builder->where('tag', $tag);
    }

    /**
     * 
     */
    public function scopeByPlanId(Builder $builder, int $planId): Builder
    {
        return $builder->where('plan_id', $planId);
    }

    /**
     * Check if is free.
     *
     * @return bool
     */
    public function isFree(): bool
    {
        return (float) $this->price <= 0.00;
    }
}
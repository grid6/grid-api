<?php

namespace Xtwoend\HySubscribe;


class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],
            ],
            'publish' => [
                [
                    'id' => 'config',
                    'description' => 'The config for subscriptions package.',
                    'source' => __DIR__ . '/../publish/subscribe.php',
                    'destination' => BASE_PATH . '/config/autoload/subscribe.php',
                ],
                [
                    'id' => 'migration',
                    'description' => 'The migrations for subscriptions package',
                    'source' => __DIR__ . '/../migrations/2022_10_27_074518_create_plan_table.php',
                    'destination' => BASE_PATH . '/migrations/2022_10_27_074518_create_plan_table.php',
                ],
                [
                    'id' => 'migration',
                    'description' => 'The migrations for subscriptions package',
                    'source' => __DIR__ . '/../migrations/2022_10_27_074750_create_plan_subscriptions_table.php',
                    'destination' => BASE_PATH . '/migrations/2022_10_27_074750_create_plan_subscriptions_table.php',
                ]
            ],
        ];
    }
}
<?php

namespace Xtwoend\HySubscribe\Trait;

use Xtwoend\HySubscribe\Period;

trait HasSubscriptionPeriod
{
    /**
     * Subscription total duration in specified interval
     * @param string $interval
     * @return int
     * @throws \Exception
     */
    public function getSubscriptionTotalDurationIn(string $interval) :int
    {
        $subscriptionPeriod = new Period($this->invoice_interval, $this->invoice_period);
        return $subscriptionPeriod->getStartDate()->{Period::diffIn($interval)}($subscriptionPeriod->getEndDate());
    }
}
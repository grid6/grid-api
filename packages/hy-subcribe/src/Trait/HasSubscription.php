<?php

namespace Xtwoend\HySubscribe\Trait;


use Carbon\Carbon;
use InvalidArgumentException;
use Xtwoend\HySubscribe\Model\Plan;
use Hyperf\Database\Model\Collection;
use Xtwoend\HySubscribe\SubscriptionPeriod;
use Hyperf\Database\Model\Relations\HasMany;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use Xtwoend\HySubscribe\Exception\DuplicateException;
use Xtwoend\HySubscribe\Exception\InvalidPlanSubscription;


trait HasSubscription
{
    /**
     * subscription
     */
    public function subscriptions(): HasMany
    {
        return $this->hasMany(PlanSubscription::class, 'user_id');
    }

    /**
     * A model may have many active subscriptions.
     *
     * @return \Hyperf\Database\Model\Collection
     */
    public function activeSubscriptions(): Collection
    {
        return $this->subscriptions->reject(function($val) {
            return $val->isInactive();
        });
    }

    /**
     * check has active subscribe
     */
    public function subscriptionActive(?string $subscriptionTag = null): bool
    {
        try {
            $subscriptionTag = $subscriptionTag ?? config('subscribe.main_subscription_tag');
            return (bool) $this->subscription($subscriptionTag)->isActive();
        } catch (\Throwable $th) {
            return false;
        }
    }

    /**
     * Get a subscription by tag.
     *
     * @param string $subscriptionTag
     *
     * @return PlanSubscription|Hyperf\Database\Model\Model|null
     */
    public function subscription(?string $subscriptionTag = null)
    {
        if ($subscriptionTag === null) {
            $count = $this->subscriptions()->count();

            if ($count === 1) {
                return $this->subscriptions()->first();
            } elseif ($count === 0) {
                throw new InvalidPlanSubscription($subscriptionTag);
            }
        }

        $subscriptionTag = $subscriptionTag ?? config('subscribe.main_subscription_tag');

        if (!$subscriptionTag) {
            throw new InvalidArgumentException('Subscription tag not provided and default config is empty.');
        }

        $subscription = $this->subscriptions()->where('tag', $subscriptionTag)->first();

        if (!$subscription) {
            throw new InvalidPlanSubscription($subscriptionTag);
        }

        return $subscription;
    }

    /**
     * Subscribe subscriber to a new plan.
     *
     * @param string $tag Identifier tag for the subscription
     * @param Plan
     * @param string|null $name Human readable name for your subscriber's subscription
     * @param string|null $description Description for the subscription
     * @param \Carbon\Carbon|null $startDate When will the subscription start
     *
     * @return Hyperf\Database\Model\Model
     * @throws \Exception
     */
    public function newSubscription(?string $tag, Plan $plan, ?string $name = null, ?string $description = null, ?Carbon $startDate = null, $paymentMethod = 'free')
    {
        $tag = $tag ?? config('subscribe.main_subscription_tag');

        $subscriptionPeriod = new SubscriptionPeriod($plan, $startDate ?? Carbon::now());

        try {
            $this->subscription($tag);
        } catch (InvalidPlanSubscription $e) {
            $subscription = $this->subscriptions()->create([
                'tag' => $tag,
                'name' => $name,
                'description' => $description,
                'plan_id' => $plan->id,
                'price' => $plan->price,
                'tier' => $plan->tier,
                'trial_interval' => $plan->trial_interval,
                'trial_period' => $plan->trial_period,
                'grace_interval' => $plan->grace_interval,
                'grace_period' => $plan->grace_period,
                'invoice_interval' => $plan->invoice_interval,
                'invoice_period' => $plan->invoice_period,
                'payment_method' => $paymentMethod,
                'trial_ends_at' => $subscriptionPeriod->getTrialEndDate(),
                'starts_at' => $subscriptionPeriod->getStartDate(),
                'ends_at' => $subscriptionPeriod->getEndDate(),
            ]);

            return $subscription;
        }

        throw new DuplicateException();
    }
}
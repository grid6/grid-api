<?php

namespace Xtwoend\HySubscribe\Exception;

class InvalidPlanSubscription extends \Exception
{
    public function __construct($subscriptionTag = "")
    {
        $message = "Subscription '{$subscriptionTag}' not found.";

        parent::__construct($message);
    }
}
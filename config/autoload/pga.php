<?php

return [
    'growinc' => [
        'host' => env('PGA_HOST'),
        'merchant_id' => env('PGA_MERCHANT_ID'),
        'merchant_secret' => env('PGA_MERCHANT_SECRET'),
        'expire_in' => 1 // in hours
    ],
    'duitku' => [
        'merchant_code' => env('DUITKU_MERCHANT_CODE'),
        'api_key' => env('DUITKU_API_KEY'),
        'expire_in' => 60 // in minutes
    ]
];
<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug');
            $table->string('name');
            $table->string('cover')->nullable();
            $table->string('cover_wide')->nullable();
            $table->integer('duration')->default(0);
            $table->string('venue')->nullable();
            $table->text('description')->nullable();
            $table->float('price', 15, 2)->default(0);
            $table->float('price_special', 15, 2)->nullable();
            $table->tinyInteger('discount')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('trainer_id')->nullable();
            $table->boolean('vt_class')->default(false);
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('classes');
    }
}

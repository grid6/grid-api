<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreatePaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->morphs('payable');
            // $table->unsignedBigInteger('package_id');
            $table->string('reference_no')->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('channel');
            $table->double('amount')->default(0);
            $table->tinyInteger('status')->default(0); // 0: pending, 1: success, 2: canceled / failed
            $table->boolean('callback_processed')->default(false);
            $table->datetime('expire_in')->nullable();
            $table->json('product')->nullable();
            $table->json('payment_create')->nullable();
            $table->json('payment_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payment_transactions');
    }
}

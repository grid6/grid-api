<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateClassesJoinedTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('classes_joined', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('class_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('schedule_id');
            $table->datetime('schedule_date')->nullable();
            $table->datetime('attended_at')->nullable();
            $table->boolean('is_waiting_list')->default(false);
            $table->timestamps();

            $table->unique(['class_id', 'user_id', 'schedule_id'], 'members_joined_classroom');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('classes_joined');
    }
}

<?php

namespace App\Task;

use Hyperf\Di\Annotation\Inject;
use Hyperf\Crontab\Annotation\Crontab;
use Hyperf\Contract\StdoutLoggerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

#[Crontab(name: "AutoRenewSubscription", rule: "0 * * * *", callback: "execute", memo: "Renew subscription charge", onOneServer: true)]
class AutoRenewSubscription
{
    #[Inject]
    private StdoutLoggerInterface $logger;

    #[Inject]
    private EventDispatcherInterface $event;

    public function execute()
    {
        $this->logger->info(date('Y-m-d H:i:s', time()));
    }

    public function charge()
    {
        //
    }
}

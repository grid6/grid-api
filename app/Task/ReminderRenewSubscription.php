<?php

namespace App\Task;

use Carbon\Carbon;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Crontab\Annotation\Crontab;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use Psr\EventDispatcher\EventDispatcherInterface;
use App\Event\ReminderRenewSubscription as ReminderEvent;

#[Crontab(name: "ReminderRenewSubscription", rule: "0 * * * *", callback: "execute", memo: "Reminder renew subscription ", onOneServer: true)]
class ReminderRenewSubscription
{
    #[Inject]
    private EventDispatcherInterface $event;

    public function execute()
    {
        // reminder last week
        $ended = Carbon::now()->addDays(7)->format('Y-m-d');
        $subscriptions = PlanSubscription::whereDate('ends_at', $ended)->get();
        foreach($subscriptions as $subscription) {
            $this->event->dispatch(new ReminderEvent($subscription, 'week'));
        }

        // reminder last day
        $ended = Carbon::now()->addDay()->format('Y-m-d');
        $subscriptions = PlanSubscription::whereDate('ends_at', $ended)->get();
        foreach($subscriptions as $subscription) {
            $this->event->dispatch(new ReminderEvent($subscription, 'day'));
        }
    }
}

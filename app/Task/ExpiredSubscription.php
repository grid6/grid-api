<?php

namespace App\Task;

use Carbon\Carbon;
use App\Xmqtt\Mqtt;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Crontab\Annotation\Crontab;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use Psr\EventDispatcher\EventDispatcherInterface;

#[Crontab(name: "ExpiredSubscription", rule: "* * * * *", callback: "execute", memo: "expired subscription membership", onOneServer: true)]
class ExpiredSubscription
{
    public function execute()
    {
        // reminder last week
        $arusr = $devs = [];
        $sqdata = PlanSubscription::where('status', 1)
                ->whereRaw("ends_at <= NOW()")
                ->get();
        if($sqdata){
          foreach($sqdata as $sqv){
            array_push($arusr, $sqv->user_id);
          }
          if(count($arusr) > 0){
            $device = (new Mqtt)->devices();
            if(isset($device['data'])){
              foreach($device['data'] as $dvc){
                array_push($devs, $dvc['id']);
              }
            }
            //check device is not empty
            if(count($devs) > 0){
                $param['customer_ids'] = $arusr;
                $param['devices'] = $devs;
                $remove = (new Mqtt)->remove_batch($param);
            }
           }
           Db::table('plan_subscriptions')
              ->where('status', '=', '1')
              ->whereRaw("ends_at < NOW()")
              ->update(['status' => '2']);

        }
    }
}

<?php

namespace App\Task;

use Hyperf\Di\Annotation\Inject;
use Hyperf\Crontab\Annotation\Crontab;
use Hyperf\Contract\StdoutLoggerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

#[Crontab(name: "ReminderClassStart", rule: "0 * * * *", callback: "execute", memo: "Reminder for class", onOneServer: true)]
class ReminderClassStart
{
    #[Inject]
    private StdoutLoggerInterface $logger;

    #[Inject]
    private EventDispatcherInterface $event;

    public function execute()
    {
        $remiderBefore = 120;


    }
}

<?php

namespace App\Task;

use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Crontab\Annotation\Crontab;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use Psr\EventDispatcher\EventDispatcherInterface;

#[Crontab(name: "PendingPaymentExpired", rule: "* * * * *", callback: "execute", memo: "Reminder renew subscription ", onOneServer: true)]
class PendingPaymentExpired
{
    public function execute()
    {
        // reminder last week
        $today = date('Y-m-d');

        Db::table('payment_transactions')
            ->where('status', '=', '0')
            ->whereRaw("DATE(expire_in) < '{$today}'")
            ->update(['status' => '2']);
    }
}

<?php

namespace App\Task;

use Carbon\Carbon;
use App\Xmqtt\Mqtt;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use App\Wallet\Wallet;
use Hyperf\Crontab\Annotation\Crontab;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use Psr\EventDispatcher\EventDispatcherInterface;

#[Crontab(name: "FreezeUser", rule: "*/3 * * * *", callback: "execute", memo: "freeze user subscription", onOneServer: true)]
class FreezeUser
{
    public function execute()
    {
        // reminder last week
        $arusr = $devs = [];
        $sqdata = Db::table("plan_subscriptions")
        ->select('user_id')
        ->leftjoin('users', 'users.id', '=', 'plan_subscriptions.user_id')
        ->whereRaw('start_freeze IS NOT NULL AND DATE(start_freeze) <= DATE(NOW()) AND users.status = 1')
        ->offset(0)
        ->limit(10)
        ->get();
        if($sqdata){
          foreach($sqdata as $sqv){
              array_push($arusr, $sqv->user_id);
              Db::table('users')
              ->select('id')
              ->where('id', $sqv->user_id)
              ->update(['status'=>'4']);
          }
          if(count($arusr) > 0){
            $device = (new Mqtt)->devices();
            if(isset($device['data'])){
              foreach($device['data'] as $dvc){
                array_push($devs, $dvc['id']);
              }
            }
            //check device is not empty
            if(count($devs) > 0){
                $param['customer_ids'] = $arusr;
                $param['devices'] = $devs;
                $remove = (new Mqtt)->remove_batch($param);
            }
          }
        }
    }
}

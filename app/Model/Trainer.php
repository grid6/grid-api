<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\Category;
use App\Model\ClassRoom;
use App\Model\TrainerLevel;
use App\Model\TrainerSchedule;
use App\Model\TrainerBooked;
use Hyperf\DbConnection\Model\Model;
/**
 */
class Trainer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'trainers';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * class relation
     */
    public function class()
    {
        return $this->hasMany(ClassRoom::class, 'trainer_id')->where('active', 1);
    }

    public function level()
    {
        return $this->belongsTo(TrainerLevel::class, 'level');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category');
    }

    /**
     * relation class schedules
     */
    public function schedules()
    {
        return $this->hasMany(TrainerSchedule::class, 'trainer_id');
    }

    public function tschedules()
    {
        return $this->hasMany(TrainerSchedule::class, 'trainer_id');
    }
    /**
     * relation class room member joined
     */
    public function booked()
    {
        return $this->belongsToMany(User::class, 'trainer_booked', 'trainer', 'user_id')
            ->withPivot('schedule_date')
            ->withTimestamps();
    }

    public function joined()
    {
        return $this->belongsToMany(User::class, 'trainer_joined', 'trainer', 'user_id')
            ->withPivot('schedule_date')
            ->withTimestamps();
    }
}

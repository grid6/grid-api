<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\Trainer;
use Hyperf\DbConnection\Model\Model;
/**
 */
class TrainerBooked extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'trainer_booked';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['trainer', 'user_id', 'sessions', 'schedule_date', 'expired_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * class relation
     */
    public function trainer()
    {
        return $this->belongsTo(Trainer::class, 'trainer');
    }
}

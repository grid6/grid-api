<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\Trainer;
use Hyperf\DbConnection\Model\Model;
/**
 */
class TrainerJoined extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'trainer_joined';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'user_id', 'schedule_id', 'trainer_id', 'attended_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
      'schedule_date' => 'date',
      'time_start' => 'time',
      'time_finish' => 'time',
    ];

    /**
     * class relation
     */
    public function trainer()
    {
        return $this->belongsTo(Trainer::class, 'trainer_id');
    }

    public function schedule()
    {
        return $this->belongsTo(TrainerSchedule::class, 'schedule_id');
    }

}

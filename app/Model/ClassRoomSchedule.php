<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\ClassRoom;
use Hyperf\DbConnection\Model\Model;
/**
 */
class ClassRoomSchedule extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'classes_schedules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'class_id', 'name', 'schedule_date', 'time_start', 'time_finish', 'full_booked', 'capacity', 'tranner'
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'schedule_date' => 'date',
        'time_start' => 'time',
        'time_finish' => 'time',
        'full_booked' => 'boolean', 
        'capacity' => 'integer'
    ];

    /**
     * relation class room member joined
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'classes_joined', 'schedule_id', 'user_id')
            ->withPivot('valid_until', 'is_waiting_list')
            ->withTimestamps();
    }

    /**
     * relation to class room
     */
    public function class()
    {
        return $this->belongsTo(ClassRoom::class, 'class_id');
    }
}
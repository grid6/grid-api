<?php

namespace App\Model\Traits;

use Carbon\Carbon;
use App\Model\ClassRoom;
use App\Model\ClassRoomSchedule;
use App\Model\Trainer;
use App\Model\TrainerSchedule;

trait CanUseClassRoom
{
    /**
     * relation to class room
     */
    public function classes()
    {
        return $this->belongsToMany(ClassRoom::class, 'classes_joined', 'user_id', 'class_id')
            ->withPivot('schedule_date', 'is_waiting_list')
            ->withTimestamps();
    }

    public function schedules()
    {
        return $this->belongsToMany(ClassRoomSchedule::class, 'classes_joined', 'user_id', 'schedule_id');
    }

    public function canUseClass(ClassRoom $class): bool
    {
        return (bool) $this->classes()->where('class_id', $class->id)->count() > 0;
    }

    public function joinClass(ClassRoomSchedule $schedule)
    {
        $isWaiting = $schedule->capacity < $schedule->members()->count();
        $schedules = explode(" ", $schedule->schedule_date);
        $schedule_date = $schedules[0];
        return $this->schedules()->attach($schedule->id, [
            'class_id' => $schedule->class->id,
            'schedule_date' => $schedule_date." ".$schedule->time_start,
            'is_waiting_list' => $isWaiting
        ]);
    }
}

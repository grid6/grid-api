<?php

namespace App\Model\Traits;

use Carbon\Carbon;
use App\Model\Trainer;
use App\Model\TrainerSchedule;

trait CanBookTrainer
{
    /**
     * relation to class room
     */
    public function trainers()
    {
        return $this->belongsToMany(Trainer::class, 'trainer_joined', 'user_id', 'trainer_id')
            ->withPivot('schedule_date')
            ->withTimestamps();
    }

    public function tschedules()
    {
        return $this->belongsToMany(TrainerSchedule::class, 'trainer_joined', 'user_id', 'schedule_id');
    }

    public function canBookPT(Trainer $trainer): bool
    {
        return (bool) $this->trainers()->where('trainer_id', $trainer->id)->count() > 0;
    }

    public function bookPT(TrainerSchedule $tschedule)
    {
        return $this->tschedules()->attach($tschedule->id, [
            'trainer_id' => $tschedule->trainer->id,
            'schedule_date' => $tschedule->schedule_date
        ]);
    }
}

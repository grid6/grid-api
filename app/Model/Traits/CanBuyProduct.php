<?php

namespace App\Model\Traits;

use Xtwoend\Wallet\Traits\HasWallet;
use Xtwoend\Wallet\Interfaces\Customer;


trait CanBuyProduct
{
    use HasWallet;

    public function canBuy(Customer $customer, int $quantity = 1, bool $force = null): bool
    {
        /**
         * If the service can be purchased once, then
         *  return !$customer->paid($this);
         */
        return true; 
    }
    
    public function getAmountProduct(Customer $customer)
    {
        return $this->price;
    }

    public function getMetaProduct(): ?array
    {
        return [
            'title' => $this->name, 
            'description' => 'Subscription of Plan #' . $this->name,
        ];
    }
    
    public function getUniqueId(): string
    {
        return (string) $this->getKey();
    }
}
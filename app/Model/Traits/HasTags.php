<?php

namespace App\Model\Traits;

use ArrayAccess;
use App\Model\Tag;
use Hyperf\Utils\Arr;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Events\Created;
use Hyperf\Database\Model\Events\Deleted;
use Hyperf\Database\Model\Relations\BelongsToMany;

trait HasTags
{
    protected array $queuedTags = [];

    public static function getTagClassName(): string
    {
        return Tag::class;
    }

    public function created(Created $event)
    {
        $taggableModel = $event->getModel();
        if (count($taggableModel->queuedTags) === 0) {
            return;
        }

        $taggableModel->attachTags($taggableModel->queuedTags);

        $taggableModel->queuedTags = [];
    }

    public function deleted(Deleted $event)
    {
        $deletedModel = $event->getModel();

        $tags = $deletedModel->tags()->get();

        $deletedModel->detachTags($tags);
    }

    public function tags(): BelongsToMany
    {
        return $this
            ->belongsToMany(Tag::class, 'taggables', 'article_id', 'tag_id')
            ->latest();
    }

    public function scopeWithAnyTags(
        Builder $query,
        string | array | ArrayAccess | Tag $tags
    ): Builder
    {
        $tags = static::convertToTags($tags);

        return $query
            ->whereHas('tags', function (Builder $query) use ($tags) {
                $tagIds = collect($tags)->pluck('id');
                $query->whereIn('tags.id', $tagIds);
            });
    }

    public function setTagsAttribute(string | array | ArrayAccess | Tag $tags)
    {   
        if (! $this->exists) {
            $this->queuedTags = $tags;
            return;
        }

        $this->syncTags($tags);
    }

    public function syncTags(string | array | ArrayAccess $tags): static
    {
        $tags = Arr::wrap($tags);
        $className = static::getTagClassName();
        $tags = collect($className::findOrCreate($tags));
        $this->tags()->sync($tags->pluck('id')->toArray());

        return $this;
    }

    protected static function convertToTags($values)
    {
        if ($values instanceof Tag) {
            $values = [$values];
        }

        return collect($values)->map(function ($value) {
            if ($value instanceof Tag) {
                return $value;
            }

            $className = static::getTagClassName();

            return $className::findFromString($value);
        });
    }

    public function attachTags(array | ArrayAccess | Tag $tags): static
    {
        $className = static::getTagClassName();

        $tags = collect($className::findOrCreate($tags));
        
        $this->tags()->syncWithoutDetaching($tags->pluck('id')->toArray());

        return $this;
    }

    public function detachTags(array | ArrayAccess $tags): static
    {
        $tags = static::convertToTags($tags);

        collect($tags)
            ->filter()
            ->each(fn (Tag $tag) => $this->tags()->detach($tag));

        return $this;
    }
}
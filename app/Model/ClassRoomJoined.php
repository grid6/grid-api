<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\ClassRoom;
use App\Model\ClassRoomSchedule;
use Hyperf\DbConnection\Model\Model;
/**
 */
class ClassRoomJoined extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'classes_joined';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'user_id', 'schedule_id', 'class_id', 'valid_until', 'attended_at', 'is_waiting_list'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'valid_until' => 'datetime',
        'attended_at' => 'datetime',
        'is_waiting_list' => 'boolean'
    ];

    /**
     * relation to class room
     */
    public function class()
    {
        return $this->belongsTo(ClassRoom::class, 'class_id');
    }

    /**
     * relation to trainer
     */
    public function schedule()
    {
        return $this->belongsTo(ClassRoomSchedule::class, 'schedule_id');
    }
}

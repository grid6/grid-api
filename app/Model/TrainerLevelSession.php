<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\TrainerLevel;
use Hyperf\DbConnection\Model\Model;
/**
 */
class TrainerLevelSession extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'trainer_level_sessions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * class relation
     */
    public function level()
    {
        return $this->belongsTo(TrainerLevel::class, 'level_id');
    }
}

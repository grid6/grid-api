<?php

declare (strict_types=1);
namespace App\Model;

use Carbon\Carbon;
use Xtwoend\Wallet\Traits\CanPay;
use Hyperf\DbConnection\Model\Model;
use Qbhy\HyperfAuth\Authenticatable;
use Xtwoend\Wallet\Traits\HasWallet;
use App\Model\Traits\CanUseClassRoom;
use App\Model\Traits\CanBookTrainer;
use Xtwoend\Wallet\Interfaces\Wallet;
use Xtwoend\Wallet\Interfaces\Customer;
use Xtwoend\HySubscribe\Trait\HasSubscription;

/**
 * User model
 */
class User extends Model implements Authenticatable, Wallet, Customer
{
    use HasSubscription, HasWallet, CanPay, CanUseClassRoom, CanBookTrainer;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone_code', 'phone', 'password', 'photo', 'gender', 'address', 'reqtopos', 'resfrompos', 'face_registered'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime'
    ];

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getPhoneFormatAttribute()
    {
        return $this->phone_code . (int) $this->phone;
    }

    public function emailVerified()
    {
        return $this->forceFill([
            'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s')
        ])->save();
    }

    public function phoneVerified()
    {
        return $this->forceFill([
            'phone_verified_at' => Carbon::now()->format('Y-m-d H:i:s')
        ])->save();
    }

    public function getId()
    {
        return $this->id;
    }

    public static function retrieveById($key): ?Authenticatable
    {
        return (new self())->find($key);
    }
}

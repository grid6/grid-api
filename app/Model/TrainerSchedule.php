<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\Trainer;
use Hyperf\DbConnection\Model\Model;
/**
 */
class TrainerSchedule extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'trainer_schedules';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'trainer_id', 'schedule_date', 'time_start', 'time_finish'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
      'schedule_date' => 'date',
      'time_start' => 'time',
      'time_finish' => 'time',
    ];

    /**
     * relation class room member joined
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'trainer_joined', 'schedule_id', 'user_id')
            ->withPivot('schedule_date')
            ->withTimestamps();
    }
    /**
     * class relation
     */
    public function trainer()
    {
        return $this->belongsTo(Trainer::class, 'trainer_id');
    }

}

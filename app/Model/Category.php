<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\ClassRoom;
use Hyperf\DbConnection\Model\Model;
/**
 */
class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'name', 'icon', 'order'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * relation to class
     */
    public function classes()
    {
        return $this->hasMany(ClassRoom::class, 'category_id');
    }
}
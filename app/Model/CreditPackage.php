<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\PaymentTransaction;
use Hyperf\DbConnection\Model\Model;
/**
 */
class CreditPackage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'credit_packages';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'price_special', 'discount', 'credit'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'credit' => 'integer'
    ];

    /**
     * morph
     */
    public function paymentTransactions()
    {
        return $this->morphOne(PaymentTransaction::class, 'payable');
    }
}
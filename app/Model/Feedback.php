<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\FeedbackSubject;
use Hyperf\DbConnection\Model\Model;

/**
 */
class Feedback extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'feedbacks';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'user_id', 'feedback_subject', 'subject', 'message'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    public function feedback_subject()
    {
        return $this->belongsTo(FeedbackSubject::class, 'feedback_subject');
    }
}

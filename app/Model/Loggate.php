<?php

declare (strict_types=1);
namespace App\Model;

use Hyperf\DbConnection\Model\Model;

/**
 */
class Loggate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'loggate';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'device_id', 'activity', 'command_id', 'request_data', 'response_data', 'status'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'request_data' => 'array',
        'response_data' => 'array',
    ];
}

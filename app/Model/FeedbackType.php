<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\FeedbackSubject;
use Hyperf\DbConnection\Model\Model;

/**
 */
class FeedbackType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'feedback_types';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'types'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    public function subject()
    {
        return $this->hasMany(FeedbackSubject::class, 'feedback_type');
    }
}

<?php

declare (strict_types=1);
namespace App\Model;

use Hyperf\DbConnection\Model\Model;
use App\Model\PersonalPackage;
/**
 */
class Personal extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'personals';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * relation class schedules
     */
    public function schedules()
    {
        return $this->hasMany(PersonalPackage::class, 'personal');
    }

    /**
     * relation class room member joined
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'personal_booked', 'personal', 'user_id')
            ->withPivot('schedule_date')
            ->withTimestamps();
    }
}

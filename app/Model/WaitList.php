<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\ClassRoomSchedule;
use Hyperf\DbConnection\Model\Model;
/**
 */
class WaitList extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'classes_waitlists';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'schedule_id', 'user_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * relation class room member joined
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'classes_joined', 'schedule_id', 'user_id')
            ->withPivot('valid_until', 'is_waiting_list')
            ->withTimestamps();
    }

    /**
     * relation to class room
     */
    public function schedule()
    {
        return $this->belongsTo(ClassRoomSchedule::class, 'schedule_id');
    }
}

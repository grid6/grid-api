<?php

declare(strict_types=1);

namespace App\Model;

use App\Model\User;
use App\Model\Trainer;
use App\Model\ClassRoomSchedule;
use App\Model\PaymentTransaction;
use Hyperf\Database\Model\Builder;
use App\Model\Traits\CanBuyProduct;
use Hyperf\DbConnection\Model\Model;
use Xtwoend\Wallet\Interfaces\Product;

class ClassRoom extends Model implements Product
{
    use CanBuyProduct;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'classes';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'default';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'name', 'cover', 'cover_wide', 'duration', 'venue', 'description', 'price', 'price_special', 'discount', 'vt_class', 'trainer_id', 'category_id', 'active'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'vt_class' => 'boolean'
    ];

    /**
     * relation to category
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * relation class room member joined
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'classes_joined', 'class_id', 'user_id')
            ->withPivot('valid_until', 'is_waiting_list')
            ->withTimestamps();
    }

    /**
     * relation class schedules
     */
    public function schedules()
    {
        return $this->hasMany(ClassRoomSchedule::class, 'class_id');
    }

    /**
     * filter only active
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('active', 1);
    }

    /**
     * trainer 
     */
    public function trainer()
    {
        return $this->belongsTo(Trainer::class, 'trainer_id');
    }

    /**
     * morph
     */
    public function paymentTransactions()
    {
        return $this->morphOne(PaymentTransaction::class, 'payable');
    }
}
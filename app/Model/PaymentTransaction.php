<?php

declare (strict_types=1);
namespace App\Model;

use Hyperf\DbConnection\Model\Model;
use Hyperf\Database\Model\Relations\MorphTo;
/**
 */
class PaymentTransaction extends Model
{
    // protected static function boot()
    // {
    //       parent::boot();
    //       self::created(function($model){
    //           // ... code here
    //           $format = str_pad((string) $model->id, 6, '0', STR_PAD_LEFT);
    //           $invoice = "CR/{$model->created_at->format('Ymd')}/{$format}";
    //           $model->update(['invoice_no' => $invoice]);
    //       });
    // }
    // public static function boot(): void
    // {
    //       parent::boot();

          // static::created(function (Model $model){
          //         $format = str_pad((string) $model->id, 6, '0', STR_PAD_LEFT);
          //         // $invoice = "CR/{$model->created_at->format('Ymd')}/{$format}";
          //         $invoice = "CR/".$model->id;
          //         $model->invoice = $invoice;
          // });
    // }
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payment_transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'payable_type', 'payable_id', 'reference_no', 'invoice_no', 'channel', 'amount', 'fee', 'status', 'callback_processed', 'payment_request', 'callback_response', 'expire_in', 'product', 'payment_create', 'payment_response'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'product' => 'array',
        'payment_create' => 'array',
        'payment_response' => 'array',
        'callback_response' => 'array',
        'expire_in' => 'datetime',
        'callback_processed' => 'boolean'
    ];

    /**
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return MorphTo
     */
    public function payable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     *
     */
    // public function created(Created $event)
    // {
    //     $model = $event->getModel();
    //     $format = str_pad((string) $model->id, 6, '0', STR_PAD_LEFT);
    //     $invoice = "CR/{$model->created_at->format('Ymd')}/{$format}";
    //
    //     $model->update(['invoice_no' => $invoice]);
    // }
}

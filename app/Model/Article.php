<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\Traits\HasTags;
use Hyperf\DbConnection\Model\Model;

/**
 */
class Article extends Model
{
    use HasTags;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'articles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'title', 'cover', 'cover_wide', 'content', 'tags'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 
    ];
}
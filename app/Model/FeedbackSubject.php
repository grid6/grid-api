<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\Feedback;
use App\Model\FeedbackType;
use Hyperf\DbConnection\Model\Model;

/**
 */
class FeedbackSubject extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'feedback_subjects';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'feedback_type', 'subject'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    public function feedback_type()
    {
        return $this->belongsTo(FeedbackType::class, 'feedback_type');
    }

    public function feedback()
    {
        return $this->hasMany(Feedback::class, 'feedback_subject');
    }
}

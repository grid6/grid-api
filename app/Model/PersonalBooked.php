<?php

declare (strict_types=1);
namespace App\Model;

use App\Model\Personal;
use App\Model\PersonalPackage;
use App\Model\Trainer;
use Hyperf\DbConnection\Model\Model;
/**
 */
class PersonalBooked extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'personal_booked';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['personal', 'user_id', 'trainer', 'schedule_id', 'schedule_date', 'attended_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
      'schedule_date' => 'datetime',
      'attended_at' => 'datetime'
    ];

    /**
     * personal relation
     */
    public function personal()
    {
        return $this->belongsTo(Personal::class, 'personal');
    }

    /**
     * relation schedules
     */
    public function schedule()
    {
        return $this->belongsTo(PersonalPackage::class, 'schedule_id');
    }

    /**
     * relation personal room member joined
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'personal_booked', 'personal', 'user_id')
            ->withPivot('schedule_date')
            ->withTimestamps();
    }

    public function trainer()
    {
        return $this->belongsTo(Trainer::class, 'trainer');
    }
}

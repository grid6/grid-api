<?php

declare (strict_types=1);
namespace App\Model;

use Hyperf\Utils\Str;
use Hyperf\Utils\Collection;
use Hyperf\DbConnection\Model\Model;

/**
 */
class Tag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tags';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'name'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * find from string
     */
    public static function findFromString(string $name)
    {
        return static::query()
            ->where("slug", Str::slug($name))
            ->first();
    }

    public static function findOrCreate(
        string | array | ArrayAccess $values
    ): Collection | Tag | static {
        $tags = collect($values)->map(function ($value) {
            if ($value instanceof self) {
                return $value;
            }

            return static::findOrCreateFromString($value);
        });

        return is_string($values) ? $tags->first() : $tags;
    }

    protected static function findOrCreateFromString(string $name)
    {
        $tag = static::findFromString($name);

        if (! $tag) {
            $tag = static::create([
                'name' => $name,
                'slug' => Str::slug($name)
            ]);
        }

        return $tag;
    }

}
<?php

declare (strict_types=1);
namespace App\Model;

use Hyperf\DbConnection\Model\Model;
/**
 */
class PersonalPackage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'personal_packages';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'schedule_date' => 'date',
        'time_start' => 'time',
        'time_finish' => 'time',
        'capacity' => 'integer'
    ];

    /**
     * personal relation
     */
    public function personal()
    {
        return $this->belongsTo(Personal::class, 'personal');
    }
}

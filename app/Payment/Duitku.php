<?php

namespace App\Payment;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Hyperf\Utils\Codec\Json;
use App\Event\PaymentSuccess;
use App\Event\PaymentCanceled;
use App\Model\PaymentTransaction;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Guzzle\CoroutineHandler;
use App\Payment\Exception\DuitkuException;
use Psr\EventDispatcher\EventDispatcherInterface;


class Duitku
{
    const URL = 'https://passport.duitku.com';
    const DEV_URL = 'https://sandbox.duitku.com';

    protected string $callbackUrl;
    protected string $returnUrl;
    protected string $merchantCode;
    protected string $apiKey;
    protected $http;

    #[Inject]
    protected EventDispatcherInterface $event;

    public function __construct() {
        $this->merchantCode = config('pga.duitku.merchant_code', "DS15000");
        // $this->merchantCode = "DS15000";
        $this->apiKey = config('pga.duitku.api_key', '8f869a65a1bf43eba6f58da6012cf0a9');
        // $this->apiKey = "8f869a65a1bf43eba6f58da6012cf0a9";
        // $this->callbackUrl = 'https://d722-124-158-148-185.ngrok.io/payment/callback';
        // $this->returnUrl = 'https://d722-124-158-148-185.ngrok.io/payment/callback';
        $bsurl = config('app_env') == 'production' ? 'xapi' : 'devapi';
        $this->callbackUrl = 'https://'.$bsurl.'.gridfitnesshub.com/payment/callback';
        $this->returnUrl = 'https://'.$bsurl.'.gridfitnesshub.com/payment/callback';
        $this->http = new Client([
            'base_uri' => config('app_env') == 'production' ? self::URL : self::DEV_URL,
            'handler' => HandlerStack::create(new CoroutineHandler()),
            'timeout' => 15,
            'debug' => true,
            'swoole' => [
                'timeout' => 20,
                'socket_buffer_size' => 1024 * 1024 * 2,
            ],
        ]);
    }

    public function channels(int $amount = 0)
    {
        $date = Carbon::now()->format('Y-m-d H:i:s');

        $data = [
            'merchantcode' => $this->merchantCode,
            'amount' => $amount,
            'datetime' => $date,
            'signature' => $this->signature([$this->merchantCode, $amount, $date, $this->apiKey])
        ];

        $result = $this->exec('POST', '/webapi/api/merchant/paymentmethod/getpaymentmethod', $data);

        return (array) $result['paymentFee'];
    }

    public function create(array $data = [])
    {
        $params = [
            'merchantCode' => $this->merchantCode,
            'paymentAmount' => $data['amount'],
            'paymentMethod' => $data['channel'],
            'merchantOrderId' => $data['invoice_no'],
            'productDetails' => $data['description'],
            'additionalParam' => [],
            'merchantUserInfo' => [],
            'customerVaName' => $data['user']['first_name'] . ' ' . $data['user']['last_name'],
            'email' => $data['user']['email'],
            'phoneNumber' =>  $data['user']['phone'],
            // 'accountLink' => $accountLink,
            'itemDetails' => (array) $data['items'],
            'customerDetail' => [
                'firstName' => $data['user']['first_name'],
                'lastName' => $data['user']['last_name'],
                'email' => $data['user']['email'],
                'phoneNumber' => $data['user']['phone'],
                'billingAddress' => $data['user']['address'],
                'shippingAddress' => $data['user']['address']
            ],
            'callbackUrl' => $this->callbackUrl,
            'returnUrl' => $this->returnUrl,
            'signature' => $this->signature([$this->merchantCode, $data['invoice_no'], $data['amount'], $this->apiKey], 'md5'),
            'expiryPeriod' => config('pga.duitku.expire_in', 60) // 1 jam default pembayaran expires
        ];

        $ptx = PaymentTransaction::where(['invoice_no'=>$data['invoice_no']])
        ->first();
        // if(isset($ptx->id)){
            $ptx->update(['payment_request'=>json_encode($params)]);
        // }

        return $this->exec('POST', '/webapi/api/merchant/v2/inquiry', $params);
    }

    public function check($reff)
    {
        $data = [
            'merchantCode' => $this->merchantCode,
            'merchantOrderId' => $reff,
            'signature' => $this->signature([$this->merchantCode, $reff, $this->apiKey], 'md5')
        ];

        $result = $this->exec('POST', '/webapi/api/merchant/transactionStatus', $data);

        $trans = PaymentTransaction::where('invoice_no', $reff)->first();

        if($result['statusCode'] == '00') {
            $trans->status = 1;
            $this->event->dispatch(new PaymentSuccess($trans));
        }

        if($result['statusCode'] == '02') {
            $trans->status = 2;
            $this->event->dispatch(new PaymentCanceled($trans));
        }

        $trans->payment_response = $result;
        $trans->save();

        return $trans;
    }

    public function callback(?array $data = [])
    {
        if(! $this->array_keys_exists(['merchantCode', 'amount', 'merchantOrderId', 'reference', 'signature'], $data)) {
            throw new DuitkuException("Bad Parameter", 400);
        }

        if($this->signature([$this->merchantCode, $data['amount'], $data['merchantOrderId'], $this->apiKey], 'md5') != $data['signature']) {
            throw new DuitkuException("Bad Signature", 400);
        }

        $trans = PaymentTransaction::where('invoice_no', $data['merchantOrderId'])->first();
        if($data['resultCode'] == '00') {
            $trans->update([
                'status' => 1,
                'payment_response' => $data,
            ]);

            $this->event->dispatch(new PaymentSuccess($trans));
        }

        if($data['resultCode'] == '02') {
            $trans->update([
                'status' => 2,
                'payment_response' => $data,
            ]);

            $this->event->dispatch(new PaymentCanceled($trans));
        }

        return $trans;
    }

    private function exec($method, $path, $data = [])
    {
        $result = $this->http->request($method, $path, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Content-Length' =>  strlen(Json::encode($data))
            ],
            'json' => $data
        ]);
        $body = (string) $result->getBody();
        $json = Json::decode($body);
        $code = (int) $result->getStatusCode();

        if($code != 200) {
            throw new DuitkuException($json->Message, $code);
        }

        return $json;
    }

    private function signature(array $data = [], $hash = 'sha256')
    {
        $data = implode('', $data);
        if($hash == 'sha256') {
            return hash('sha256', $data);
        }else{
            return md5($data);
        }
    }

    protected function array_keys_exists(array $keys, array $arr)
    {
        return !array_diff_key(array_flip($keys), $arr);
    }
}

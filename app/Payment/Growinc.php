<?php

namespace App\Payment;

use GuzzleHttp\Client;
use App\Event\PaymentSuccess;
use App\Model\PaymentTransaction;
use Hyperf\Di\Annotation\Inject;
use App\Payment\Exception\DuplicateExeption;
use Psr\EventDispatcher\EventDispatcherInterface;

class Growinc
{
    protected $client;
    protected $merchant_code;
    protected $merchant_secret;

    #[Inject]
    protected EventDispatcherInterface $event;

    public function __construct() {
        $this->merchant_secret = config('pga.growinc.merchant_secret');
        $this->merchant_code = config('pga.growinc.merchant_id');
        $this->client = new Client([
            'base_uri' => config('pga.growinc.host'),
            'timeout'  => 2.0,
        ]);
    }

    public function signature($code=null)
    {
        $node = is_null($code)? $this->merchant_code : $this->merchant_code . ':' . $code;
        return hash_hmac('sha256', $node, $this->merchant_secret);
    }

    public function create($data)
    {
        $data = [
            'merchant_code' => $this->merchant_code,
            'invoice_no' => $data['invoice_no'],
            'payment_method_code' => $data['payment_method_code'],
            'expire_in' => config('pga.growinc.expire_in'),
            'amount' => $data['amount'],
            'description' => $data['description'],
            'signature' => $this->signature($data['invoice_no']),
            'customer_name' => $data['user']['name'],
            'customer_email' => $data['user']['email'],
            'customer_phone' => $data['user']['phone'],
            'customer_address' => $data['user']['address'],
        ];

        $result = $this->exec('/api/pay/create', $data);

        if(is_string($result)) {
            throw new DuplicateExeption($result, 422);
            
        }

        return $result;
    }

    public function callback($data)
    {
        if(! isset($data['reference_no'])) {
            return [
                'error' => 422,
                'message' => 'reference_no required'
            ];
        }
       
        $trans = PaymentTransaction::where('reference_no', $data['reference_no'])->first();
        if(! $trans) {
            return [
                'error' => 404,
                'message' => 'transaction not found'
            ];
        }

        if(! isset($data['signature']) || ! $this->checkSignature($data['signature'], $trans)) {
            return [
                'error' => 422,
                'message' => 'invalid signature'
            ];
        }

        $status = $data['paid_status'];

        $trans->update([
            'status' => $status,
            'payment_response' => $data,
        ]);

        if($trans->status == 1 && $data['paid_status'] == 1) {
            $this->event->dispatch(new PaymentSuccess($trans)); 
        }

        return [
            'success' => true,
            'error' => 0,
            'data' => $trans
        ];
    }

    public function check($reff)
    {
        $data = [
            'merchant_code' => $this->merchant_code,
            'reference_no' => $reff,
            'signature' => $this->signature($reff)
        ];

        return $this->exec('/api/pay/check', $data);
    }

    public function channels()
    {
        $data = [
            'merchant_code' => $this->merchant_code,
            'signature' => $this->signature()
        ];
        return $this->exec('/api/payment_methods', $data);
    }

    private function checkSignature($singature, $transaction)
    {
        return ($singature === $this->signature($transaction->invoice_no));
    }

    public function exec($path, $data = [])
    {
        $response = $this->client->post($path, [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'json' => $data
        ]);

        $body = (string) $response->getBody();
        $data = json_decode($body);
        
        if($data->status != '000') {
            return $data->error_message;
        }

        return $data->data;
    }
}
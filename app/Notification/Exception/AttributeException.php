<?php

namespace App\Notification\Exception;


class AttributeException extends \RuntimeException
{
    public function __construct($attribute) {
        $message = __('Attribute required');
        parent::__construct($message, 100004);
    }
}
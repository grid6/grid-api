<?php

namespace App\Notification;

use Kreait\Firebase\Factory;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Messaging\RawMessageFromArray;
use App\Notification\Exception\CouldNotSendNotification;

class Fcm
{
    const MAX_TOKEN_PER_REQUEST = 500;

    protected $recipients;
    protected $topic;
    protected $data;
    protected $notification;

    public function to(array|string $recipients)
    {
        $this->recipients = $recipients;
        return $this;
    }

    public function toTopic(string $topic)
    {
        $this->topic = $topic;
        return $this;
    }

    public function data(array $data = [])
    {
        $this->data = $data;
        return $this;
    }

    public function notification(array $notification = [])
    {
        $this->notification = $notification;

        return $this;
    }

    public function send()
    {
        $payloads = [
            'notification' => $this->notification,
            'data' => $this->data ?? [],
        ];

        if($this->topic) {
            $payloads['topic'] = $this->topic;
        }else{
            $payloads['token'] = $this->recipients;
        }

        $message = new RawMessageFromArray($payloads);
        $responses = [];

        try {
            if (is_array($this->recipients)) {
                $partialTokens = array_chunk($this->recipients, self::MAX_TOKEN_PER_REQUEST, false);
                foreach ($partialTokens as $tokens) {
                    $responses[] = $this->messaging()->sendMulticast($message, $tokens);
                }
            } else {
                $responses[] = $this->messaging()->send($message);
            }
        } catch (MessagingException $th) {
            throw CouldNotSendNotification::serviceRespondedWithAnError($th);
        }

        return $responses;
    }

    public function subTopic(array|string $recipients, string $topic)
    {
        $responses = [];
        try {
          $responses[] = $this->messaging()->subscribeToTopic($topic, $recipients);
        } catch (MessagingException $th) {
            throw CouldNotSendNotification::serviceRespondedWithAnError($th);
        }
        return $responses;
    }

    public function unsubTopic(array|string $recipients, string $topic)
    {
        $responses = [];
        try {
          $responses[] = $this->messaging()->unsubscribeFromTopic($topic, $recipients);
        } catch (MessagingException $th) {
            throw CouldNotSendNotification::serviceRespondedWithAnError($th);
        }

        return $responses;
    }

    protected function messaging(): Messaging
    {
        $configPath =  config('fcm.file');

        if(! file_exists($configPath)) {
            throw new \Exception('FCM file not found, please check your config.');
        }
        $factory = (new Factory)->withServiceAccount($configPath);

        return $factory->createMessaging();
    }
}

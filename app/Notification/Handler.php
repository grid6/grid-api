<?php

namespace App\Notification;

use App\Model\User;
use Ramsey\Uuid\Uuid;
use App\Notification\Fcm;
use App\Model\Notification;
use App\Notification\Exception\AttributeException;

class Handler
{
    protected array $attributes;
    protected ?string $topic;

    /**
     * format
     * {
     *  "to": "", // int|array
     *  "activity": "", // string
     *  "channels": "", // array
     *  "title": "", // string
     *  "body": "", // string
     *  "image": "", // string
     *  "data": "" // array
     * }
     */
    public function __construct(array $attributes = []) {
        $this->attributes = $attributes;
    }

    public function __set($key, $value)
    {
        $this->attributes[$key] = $value;
        return $this;
    }

    public function __get($key)
    {
        return $this->attributes[$key] ?? null;
    }

    public function getData(): ?array
    {
        return $this->attributes['data'] ?? null;
    }
    
    public function send()
    {
        $title = $this->title;
        $body = $this->body;
        
        if(! $this->array_keys_exists(['title', 'body', 'to', 'channels'], $this->attributes)) {
            throw new AttributeException('Title Or Body');
        }

        $recipients = is_array($this->to)? $this->to : [$this->to];
        $channels = $this->channels;

        $responses = [];
        // insert notif to db
        $activity = $this->attributes['activity'] ?? 'notification';
        foreach($recipients as $recipient) {
            Notification::create([
                'id' => Uuid::uuid4()->toString(),
                'user_id' => $recipient,
                'activity' => $activity,
                'data' => [
                    'activity' => $activity,
                    'data' => $this->attributes,
                    'link' => $this->attributes['link'] ?? null,
                ],
                'link' => $this->attributes['link'] ?? null,
                'read_at' => null,
            ]);

            // send to topic user
            $fcm = (new Fcm)->toTopic('user-'.$recipient)
                ->data($this->attributes['data'] ?? [])
                ->notification([
                    'title' => $this->attributes['title'],
                    'body' => $this->attributes['body'],
                    'image' => $this->attributes['image'] ?? null
                ])
                ->send();

            $responses[] = $fcm;
        }

        return $responses;
    }

    public function sendBroadcast(?string $topic = null)
    {
        $this->topic = $topic;
        
        $title = $this->title;
        $body = $this->body;
        
        if(! $this->array_keys_exists(['title', 'body'], $this->attributes)) {
            throw new AttributeException('Title Or Body');
        }

        // send to topic user
        $fcm = (new Fcm)->toTopic($this->topic)
            ->data($this->attributes['data'] ?? [])
            ->notification([
                'title' => $this->attributes['title'],
                'body' => $this->attributes['body'],
                'image' => $this->attributes['image'] ?? null
            ])
            ->send();

        return $fcm;
    }

    public function toArray(): array
    {
        return (array) $this->attributes;
    }

    protected function array_keys_exists(array $keys, array $arr) 
    {
        return !array_diff_key(array_flip($keys), $arr);
    }
}
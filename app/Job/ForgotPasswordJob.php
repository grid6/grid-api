<?php

declare(strict_types=1);

namespace App\Job;

use App\Model\User;
use App\Mail\ForgotPassword;
use Hyperf\AsyncQueue\Job;
use HyperfExt\Mail\PendingMail;

class ForgotPasswordJob extends Job
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function handle()
    {
        $email = new ForgotPassword($this->user);
        $email->to($this->user->email, $this->user->first_name)
              ->subject('Forgot Password');

        $mailer = make(PendingMail::class);
        $mailer->send($email);
    }
}

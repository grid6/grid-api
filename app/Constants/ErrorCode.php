<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

#[Constants]
class ErrorCode extends AbstractConstants
{
    /**
     * @Message("Server Error！")
     */
    public const SERVER_ERROR = 500;

    /**
     * @Message("Form Error！")
     */
    public const FORM_ERROR = 422;

    /**
     * @Message("Unauthorize！")
     */
    public const UNAUTHORIZE = 401;

    /**
     * @Message("Forbidden access!")
     */
    public const FORBIDDEN = 403;
    /**
     * @Message("Not Found!")
     */
    public const DATA_NOTFOUND = 404;
}

<?php

namespace App\Wallet;

use Ramsey\Uuid\Uuid;
use Hyperf\DbConnection\Model\Model;
use Hyperf\Database\Model\Events\Creating;

class Transaction extends Model
{
    const TYPES = ['deposit', 'withdraw', 'fee', 'tax'];

    protected $table = 'transactions';
    
    protected $fillable = [
        'wallet_id', 'type', 'amount', 'balance', 'meta', 'spend', 'uuid', 'confirmed'
    ];

    protected $casts = [
        'confirmed' => 'boolean',
        'amount' => 'float',
        'balance' => 'float',
        'meta' => 'array',
        'spend' => 'boolean'
    ];

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }

    public function creating(Creating $event)
    {
        $this->spend = ($this->type != 'deposit') ? 0 : 1;
        $this->uuid = Uuid::uuid4()->toString();
    }
}
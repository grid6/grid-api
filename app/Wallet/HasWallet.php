<?php

namespace App\Wallet;

use App\Wallet\Transaction;
use App\Wallet\Wallet;
use Hyperf\DbConnection\Db;

trait HasWallet
{
    public function wallet()
    {
        return $this->hasOne(Wallet::class, 'user_id');
    }

    public function getBalanceAttribute()
    {
        return $this->wallet->balance;
    }

    /**
     * The input means in the system.
     *
     * @param int|string $amount
     * @param array|null $meta
     * @param bool $confirmed
     *
     * @return Transaction
     *
     * @throws AmountInvalid
     * @throws Throwable
     */
    public function deposit($amount, ?array $meta = null, bool $confirmed = true): Transaction
    {
        $self = $this;
        Db::transaction(function () use ($self, $amount, $meta, $confirmed) {
            Transaction::create([
                'amount' => (float) $amount,
                'balance' => (float) $self->balance,
                'meta' => $meta,
                'confirmed' => $confirmed
            ]);
            $self->wallet()->increment('balance', $amount);
        });
    }

    public function withdraw($amount, ?array $meta = null, bool $confirmed = true): Transaction
    {
        $self = $this;
        Db::transaction(function () use ($self, $amount, $meta, $confirmed) {
            Transaction::create([
                'amount' => (float) $amount,
                'balance' => (float) $self->balance,
                'meta' => $meta,
                'confirmed' => $confirmed
            ]);
            $self->wallet()->decrement('balance', $amount);
        });
    }
}

<?php

namespace App\Wallet;

use App\Model\User;
use Ramsey\Uuid\Uuid;
use App\Wallet\Transaction;
use Hyperf\DbConnection\Model\Model;
use Hyperf\Database\Model\Events\Creating;

class Transfer extends Model
{
    protected $table = 'transfers';
    
    protected $fillable = [
        'deposit_id',
        'withdraw_id',
        'from', 
        'to', 
        'amount', 
        'meta', 
        'fee', 
        'uuid',
        'confirmed',
    ];

    public function creating(Creating $event)
    {
        $this->uuid = Uuid::uuid4()->toString();
    }

    public function from()
    {
        return $this->belongsTo(User::class, 'from');
    }

    public function to()
    {
        return $this->belongsTo(User::class, 'to');
    }

    public function deposit()
    {
        return $this->belongsTo(Transaction::class, 'deposit_id');
    }

    public function withdraw()
    {
        return $this->belongsTo(Transaction::class, 'withdraw_id');
    }
}
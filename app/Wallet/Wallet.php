<?php

namespace App\Wallet;

use App\Model\User;
use Hyperf\DbConnection\Model\Model;

class Wallet extends Model
{
    protected $table = 'wallets';

    protected $fillable = [
        'user_id',
        'balance',
        'is_lock',
        'removed'
    ];

    protected $casts = [
        'is_lock' => 'boolean'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'wallet_id');
    }
}

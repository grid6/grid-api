<?php

namespace App\Event;

use Xtwoend\HySubscribe\Model\PlanSubscription;

class ReminderRenewSubscription
{
    public PlanSubscription $subscription;
    public $priode;

    public function __construct($subscription, $priode = 'week') {
        $this->subscription = $subscription;
        $this->priode = $priode;
    }
}
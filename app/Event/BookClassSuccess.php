<?php

namespace App\Event;

use App\Model\ClassRoomJoined;

class BookClassSuccess
{
    public ClassRoomJoined $course;
    public function __construct($course) {
        $this->course = $course;
    }
}

<?php

namespace App\Event;

use App\Model\PaymentTransaction;

class PaymentSuccess
{
    public PaymentTransaction $transaction;
    public function __construct($transaction) {
        $this->transaction = $transaction;
    }
}
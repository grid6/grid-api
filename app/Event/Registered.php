<?php

namespace App\Event;

use App\Model\User;

class Registered
{
    public User $user;
    public function __construct($user) {
        $this->user = $user;
    }
}
<?php

namespace App\Event;

use App\Model\PaymentTransaction;

class PaymentCanceled
{
    public PaymentTransaction $transaction;
    public function __construct($transaction) {
        $this->transaction = $transaction;
    }
}
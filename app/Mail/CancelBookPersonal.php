<?php

declare(strict_types=1);
/**
 * This file is part of hyperf-ext/mail.
 *
 * @link     https://github.com/hyperf-ext/mail
 * @contact  eric@zhu.email
 * @license  https://github.com/hyperf-ext/mail/blob/master/LICENSE
 */
namespace App\Mail;

use App\Model\Personal;
use App\Model\User;
use HyperfExt\Contract\ShouldQueue;
use HyperfExt\Mail\Mailable;
use HyperfExt\Mail\Transport\PostmarkTransport;

class CancelBookPersonal extends Mailable
{
    public $user;
    public $personal;
    /**
     * Create a new message instance.
     */
    public function __construct(User $user, Personal $personal)
    {
        //
        $this->user = $user;
        $this->personal = $personal;
    }

    /**
     * Build the message.
     */
    public function build(): void
    {
        $this->htmlView('emails.cancel_personal');
    }
}

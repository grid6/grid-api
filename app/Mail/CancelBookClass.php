<?php

declare(strict_types=1);
/**
 * This file is part of hyperf-ext/mail.
 *
 * @link     https://github.com/hyperf-ext/mail
 * @contact  eric@zhu.email
 * @license  https://github.com/hyperf-ext/mail/blob/master/LICENSE
 */
namespace App\Mail;

use App\Model\User;
use App\Model\ClassRoom;
use HyperfExt\Contract\ShouldQueue;
use HyperfExt\Mail\Mailable;
use HyperfExt\Mail\Transport\PostmarkTransport;

class CancelBookClass extends Mailable
{
    public $user;
    public $course;
    public $price;
    /**
     * Create a new message instance.
     */
    public function __construct(User $user, ClassRoom $course, int $price)
    {
        //
        $this->user = $user;
        $this->course = $course;
        $this->price = $price;
    }

    /**
     * Build the message.
     */
    public function build(): void
    {
        $this->htmlView('emails.cancel_class')
        ->with([
            'date' => date("Y-m-d"),
            'strtotimedate' => strtotime(date("Y-m-d H:i:s")),
            'users' => $this->user,
            'credits' => $this->price,
            'courses' => $this->course
        ]);
    }
}

<?php

declare(strict_types=1);
/**
 * This file is part of hyperf-ext/mail.
 *
 * @link     https://github.com/hyperf-ext/mail
 * @contact  eric@zhu.email
 * @license  https://github.com/hyperf-ext/mail/blob/master/LICENSE
 */
namespace App\Mail;

use App\Model\User;
use App\Model\Transaction;
use App\Model\ClassRoom;
use HyperfExt\Contract\ShouldQueue;
use HyperfExt\Mail\Mailable;
use HyperfExt\Mail\Transport\PostmarkTransport;

class PurchaseCourse extends Mailable
{
    /**
     * Create a new message instance.
     */
    public $transaction;
    public $user;

    public function __construct(User $user, Transaction $transaction)
    {
        //
        $this->user = $user;
        $this->transaction = $transaction;
    }

    /**
     * Build the message.
     */
    public function build(): void
    {
        $class = ClassRoom::find($this->transaction->meta["class_id"]);

        $this->htmlView('emails.purchase_course')
        ->with([
            'users'=>$this->user,
            'transactions'=>$this->transaction,
            'class'=>$class
        ]);
    }
}

<?php

declare(strict_types=1);
/**
 * This file is part of hyperf-ext/mail.
 *
 * @link     https://github.com/hyperf-ext/mail
 * @contact  eric@zhu.email
 * @license  https://github.com/hyperf-ext/mail/blob/master/LICENSE
 */
namespace App\Mail;

use Carbon\Carbon;
use App\Model\User;
use App\Model\PaymentTransaction;
use App\Model\ClassRoomJoined;
use App\Model\ClassRoomSchedule;
use App\Model\ClassRoom;
use HyperfExt\Contract\ShouldQueue;
use HyperfExt\Mail\Mailable;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use HyperfExt\Mail\Transport\PostmarkTransport;

class BookReminder extends Mailable
{
    public $user;
    public $course;
    /**
     * Create a new message instance.
     */

    public function __construct(User $user, ClassRoomJoined $course)
    {
        //
        $this->user = $user;
        $this->course = $course;
    }

    /**
     * Build the message.
     */
    public function build(): void
    {
        $scheds = ClassRoomSchedule::find($this->course->schedule_id);
        $class = ClassRoom::find($this->course->class_id);
        $this->htmlView('emails.reminder_book')
        ->textView('emails.reminder_book_plain')
        ->with([
            'name' => $this->user->first_name." ".$this->user->last_name,
            'coursename' => $class->name,
            'transactions'=> $this->course,
            'schedules'=> $scheds
        ]);
    }
}

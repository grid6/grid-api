<?php

declare(strict_types=1);
/**
 * This file is part of hyperf-ext/mail.
 *
 * @link     https://github.com/hyperf-ext/mail
 * @contact  eric@zhu.email
 * @license  https://github.com/hyperf-ext/mail/blob/master/LICENSE
 */
namespace App\Mail;

use App\Model\User;
use HyperfExt\Mail\Mailable;
use HyperfExt\Contract\ShouldQueue;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use HyperfExt\Mail\Transport\PostmarkTransport;

class ReminderRenewSubscription extends Mailable
{
    public $user;

    public $subscription;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user, PlanSubscription $subscription)
    {
        $this->user = $user;
        $this->subscription = $subscription;
    }

    /**
     * Build the message.
     */
    public function build(): void
    {
        $this->htmlView('emails.reminder_subscription');
    }
}

<?php

declare(strict_types=1);
/**
 * This file is part of hyperf-ext/mail.
 *
 * @link     https://github.com/hyperf-ext/mail
 * @contact  eric@zhu.email
 * @license  https://github.com/hyperf-ext/mail/blob/master/LICENSE
 */
namespace App\Mail;

use App\Model\User;
use App\Model\PaymentTransaction;
use HyperfExt\Contract\ShouldQueue;
use HyperfExt\Mail\Mailable;
use HyperfExt\Mail\Transport\PostmarkTransport;

class PurchaseTrainer extends Mailable
{
    public $user;

    public $transaction;
    /**
     * Create a new message instance.
     */
    public function __construct(User $user, PaymentTransaction $transaction)
    {
        //
        $this->user = $user;
        $this->transaction = $transaction;
    }

    /**
     * Build the message.
     */
    public function build(): void
    {
        switch($this->transaction->channel){
            case "CASH" : $channel="Cash";break;
            case "BC" : $channel="Virtual Account BCA";break;
            case "BR" : $channel="Virtual Account BRI";break;
            case "NC" : $channel="Virtual Account BNC";break;
            case "NQ" : $channel="Virtual Account Nobu QRIS";break;
            case "VA" : $channel="Virtual Account Maybank";break;
            case "BT" : $channel="Virtual Account Permata";break;
            case "B1" : $channel="Virtual Account Cimb Niaga";break;
            case "A1" : $channel="Virtual Account ATM Bersama";break;
            case "I1" : $channel="Virtual Account BNI";break;
            case "M1" : $channel="Virtual Account Mandiri";break;
            case "M2" : $channel="Virtual Account Mandiri H2H";break;
            case "OV" : $channel="OVO";break;
            case "OL" : $channel="OVO Link";break;
            case "SP" : $channel="Shopeepay QRIS";break;
            case "SA" : $channel="Shopeepay Apps";break;
            case "SL" : $channel="Shopeepay Link";break;
            case "DN" : $channel="Indodana Paylater";break;
            case "LA" : $channel="LinkAja";break;
            case "VC" : $channel="Credit Card";break;
            case "FT" : $channel="Retail";break;
            case "IR" : $channel="Indomaret";break;
            default : $channel = "Virtual Account BCA";break;
        }
        $vaNumber = (isset($this->transaction->payment_create['vaNumber'])) ? ' '.$this->transaction->payment_create['vaNumber'] : '';
        $this->htmlView('emails.purchase_trainer')
        ->with([
          'users' => $this->user,
          'transactions' => $this->transaction,
          'produk' => $this->transaction->product,
          'channel' => $channel,
          'vaNumber' => $vaNumber
        ]);
    }
}

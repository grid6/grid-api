<?php

declare(strict_types=1);
/**
 * This file is part of hyperf-ext/mail.
 *
 * @link     https://github.com/hyperf-ext/mail
 * @contact  eric@zhu.email
 * @license  https://github.com/hyperf-ext/mail/blob/master/LICENSE
 */
namespace App\Mail;

use App\Model\User;
use HyperfExt\Mail\Mailable;
use HyperfExt\Contract\ShouldQueue;
use HyperfExt\Mail\Transport\PostmarkTransport;

class ForgotPassword extends Mailable
{
    /**
     * user data
     */
    public $user;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     */
    public function build(): void
    {
        $urlprod = 'https://landing.gridfitnesshub.com/forgot/password';
        $urldev = 'https://dev-landing.gridfitnesshub.com/forgot/password';
        $urlanding = config('app_env') == 'production' ? $urlprod : $urldev;
        $this->subject('Forgot Password')
        ->htmlView('emails.forgot_password')
        ->with([
            'urlcode'=>base64_encode($this->user->email),
            'urlbase' => $urlanding
        ]);
    }
}

<?php

declare(strict_types=1);
/**
 * This file is part of hyperf-ext/mail.
 *
 * @link     https://github.com/hyperf-ext/mail
 * @contact  eric@zhu.email
 * @license  https://github.com/hyperf-ext/mail/blob/master/LICENSE
 */
namespace App\Mail;

use App\Model\User;
use App\Model\PaymentTransaction;
use App\Model\TrainerJoined;
use App\Model\TrainerSchedule;
use App\Model\Trainer;
use HyperfExt\Contract\ShouldQueue;
use HyperfExt\Mail\Mailable;
use HyperfExt\Mail\Transport\PostmarkTransport;

class BookTrainer extends Mailable
{
    public $user;
    public $joined;
    /**
     * Create a new message instance.
     */
    public function __construct(User $user, TrainerJoined $joined)
    {
        //
        $this->user = $user;
        $this->joined = $joined;
    }

    /**
     * Build the message.
     */
    public function build(): void
    {
        $trainer = Trainer::find($this->joined->trainer_id);
        $sched = TrainerSchedule::find($this->joined->schedule_id);
        $this->htmlView('emails.book_trainer')
        ->textView('emails.book_trainer_plain')
        ->with([
            'name' => $this->user->first_name." ".$this->user->last_name,
            'transactions'=>$this->joined,
            'trainer' => $trainer,
            'schedule' => $sched,
            'users' => $this->user
        ]);
    }
}

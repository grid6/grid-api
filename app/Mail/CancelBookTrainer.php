<?php

declare(strict_types=1);
/**
 * This file is part of hyperf-ext/mail.
 *
 * @link     https://github.com/hyperf-ext/mail
 * @contact  eric@zhu.email
 * @license  https://github.com/hyperf-ext/mail/blob/master/LICENSE
 */
namespace App\Mail;

use App\Model\User;
use App\Model\TrainerSchedule;
use App\Model\Trainer;
use HyperfExt\Contract\ShouldQueue;
use HyperfExt\Mail\Mailable;
use HyperfExt\Mail\Transport\PostmarkTransport;

class CancelBookTrainer extends Mailable
{
    public $user;
    public $trainer;
    /**
     * Create a new message instance.
     */
    public function __construct(User $user, TrainerSchedule $trainer)
    {
        //
        $this->user = $user;
        $this->trainer = $trainer;
    }

    /**
     * Build the message.
     */
    public function build(): void
    {
        $trainers = Trainer::find($this->trainer->trainer_id);
        $this->htmlView('emails.cancel_trainer')
        ->with([
            'trainerz'=>$trainers,
            'sched'=>$this->trainer,
            'users'=>$this->user
        ]);
    }
}

<?php

declare(strict_types=1);
/**
 * This file is part of hyperf-ext/mail.
 *
 * @link     https://github.com/hyperf-ext/mail
 * @contact  eric@zhu.email
 * @license  https://github.com/hyperf-ext/mail/blob/master/LICENSE
 */
namespace App\Mail;

use HyperfExt\Contract\ShouldQueue;
use HyperfExt\Mail\Mailable;
use HyperfExt\Mail\Transport\PostmarkTransport;

class VerifyEmail extends Mailable //implements ShouldQueue
{
    public $user;

    /**
     * Create a new message instance.
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     */
    public function build(): void
    {
        $this->htmlView('emails.verify_email')
        ->with([
          'urlcode'=>base64_encode($this->user->email)
        ]);
    }
}

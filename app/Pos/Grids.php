<?php

namespace App\Pos;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Hyperf\Utils\Codec\Json;
use App\Model\User;
use App\Model\Page;
use App\Model\PaymentTransaction;
use App\Model\TrainerLevelSession;
use App\Model\CreditPackage;
use Xtwoend\HySubscribe\Model\Plan;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Guzzle\CoroutineHandler;
use App\Pos\Exception\GridsException;
use Psr\EventDispatcher\EventDispatcherInterface;


class Grids
{
    const URL = 'http://202.157.189.52:9021';
    const DEV_URL = 'http://202.157.189.52:8978';

    protected string $admin;
    protected string $adminPass;
    protected $http;

    #[Inject]
    protected EventDispatcherInterface $event;

    public function __construct() {
        $this->admin = "admin";
        $this->adminPass = "Admin!1234#";
        $this->http = new Client([
            'base_uri' => config('pos.url', self::DEV_URL),
            'handler' => HandlerStack::create(new CoroutineHandler()),
            'timeout' => 15,
            'debug' => true,
            'swoole' => [
                'timeout' => 20,
                'socket_buffer_size' => 1024 * 1024 * 2,
            ],
        ]);
    }

    public function login()
    {
        $date = Carbon::now()->format('Y-m-d H:i:s');

        $data = [
            'username' => config('pos.user', $this->admin),
            'password' => config('pos.password', $this->adminPass)
        ];

        $result = $this->exec_form('POST', '/api/Account', $data);

        return (array) $result;
    }

    public function createCust(array $data = [])
    {
        $log = $this->login();

        if(!isset($log["token"])){
            throw new GridsException("token are required", 400);
        }

        $token = $log["token"];

        $params = [
            'fcustno' => 'APP'.sprintf("%04s", $data['id']),
            'fcustname' => $data['first_name'] . ' ' . $data['last_name'],
            'fnpwp' => (isset($data['npwp'])) ? $data['npwp'] : '',
            'faddress1' => (isset($data['address'])) ? $data['address'] : 'Alamat 1',
            'faddress2' => (isset($data['address2'])) ? $data['address2'] : 'Alamat 2',
            'fcity' => (isset($data['city'])) ? $data['city'] : 'Jakarta',
            'ffax' => (isset($data['fax'])) ? $data['fax'] : '022',
            'ftaxaddress1' => (isset($data['taxaddress1'])) ? $data['taxaddress1'] : 'Alamat 3',
            'ftaxaddress2' => (isset($data['taxaddress2'])) ? $data['taxaddress2'] : 'Alamat 4',
            'ftaxcity' => (isset($data['taxcity'])) ? $data['taxcity'] : 'Jkt',
            'femail' => $data['email'],
            'ftelp' =>  $data['phone_code'].$data['phone'],
            'fwil' => (isset($data['wil'])) ? $data['wil'] : '0',
            'fkirimcity' => (isset($data['kirimcity'])) ? $data['kirimcity'] : 'Jkt-3',
            'flimit' => (isset($data['limit'])) ? $data['limit'] : 0,
            'fkontakperson' => (isset($data['kontakperson'])) ? $data['kontakperson'] : '',
            'fjabatan' => (isset($data['jabatan'])) ? $data['jabatan'] : '',
            'fsales' => (isset($data['sales'])) ? $data['sales'] : '',
            'fkodefp' => (isset($data['kodefp'])) ? $data['kodefp'] : '0',
            'sfexpdate' => (isset($data['expdate'])) ? $data['expdate'] : date('Y-m-d', strtotime('+1 year')),
            'fblokir' => (isset($data['blokir'])) ? $data['blokir'] : '0',
            'fnik' => (isset($data['nik'])) ? $data['nik'] : 'NIK',
            'sftgllahir' => (isset($data['birthdate'])) ? $data['birthdate'] : date('Y-m-d'),
            'fkelamin' => (isset($data['gender'])) ? $data['gender'] : 'L'
        ];


        return $this->exec('POST', '/api/Customers', $params, $token);
    }

    public function createTrans(array $data = [])
    {
        $log = $this->login();

        if(!isset($log["token"])){
            throw new GridsException("token are required", 400);
        }
        $token = $log["token"];
        $name = $cu = "";
        $usr = null;
        if(isset($data["user_id"])){
          $cu = "APP".sprintf("%04s",$data['user_id']);
          $usr = User::find($data["user_id"]);
          if(isset($usr->first_name)){
              $name = $usr->first_name." ".$usr->last_name;
          }
        }

        $code = "";
        $taxx = 0;
        if(isset($data["product"])){
            $prdid = $data["product"]["id"];
            if(isset($data["product"]["tag"])){
                $plan = Plan::find($prdid);
                if(isset($plan->kode_menu)){
                  $code = $plan->kode_menu;
                  $taxx= $plan->tax;
                }
                else $code = "GRID.09.0001";
            }if(isset($data["product"]["level"])){
                $dwhr = [
                  'level_id' => $data["product"]["level"]["id"],
                  'sessions' => $data["product"]["sessions"],
                ];
                $trlev = TrainerLevelSession::where($dwhr)->first();
                if(isset($trlev->kode_menu)){
                    $code = $trlev->kode_menu;
                    $taxx= $trlev->tax;
                }
                else $code = "GRID.06.0001";
            }if(isset($data["product"]["credit"])){
                $cred = CreditPackage::find($prdid);
                if(isset($cred->kode_menu)){
                  $code = $cred->kode_menu;
                  $taxx = $cred->tax;
                }else $code = "GRID.19.0001";
            }else{
                $code = "GRID.09.0001";
            }
        }

        $now = date("Y-m-d H:i:s");
        $ctz = (isset($data['created_at'])) ? $data['created_at'] : $now;
        $amount = (isset($data['amount'])) ? $data['amount'] : 0;
        $amountgross = $amount - $taxx;

        $data_master = [
          'fsono' => "APP.23.".sprintf("%04s", $data['id']),
          'fnama' => $name,
          'ftaxno' => (isset($data['taxno'])) ? $data['taxno'] : '',
          'frefno' => (isset($data['reference_no'])) ? $data['reference_no'] : '',
          'fcustno' => $cu,
          'fsalesman' => (isset($data['salesman'])) ? $data['salesman'] : '',
          'fdiscpersen' => (isset($data['discpersen'])) ? $data['discpersen'] : 0,
          'fdiscount' => (isset($data['discount'])) ? $data['discount'] : 0,
          'fdatetime' => $ctz,
          'fongkosangkut' => (isset($data['ongkos'])) ? $data['ongkos'] : 0,
          'fprint' => (isset($data['print'])) ? $data['print'] : null,
          'ftypemember' => "1",
          'ftypesales' =>  (isset($data['typesales'])) ? "1" : "0",
          'famountgross' => $amountgross,
          'famountpajak' => $taxx,
          'famountso' => (isset($data['amount'])) ? $data['amount'] : 0,
          'famountremain' => 0,
          'fcash' => (isset($data['cash'])) ? $data['cash'] : '0',
          'fgrosir' => (isset($data['grosir'])) ? $data['grosir'] : '0',
          'fcabang' => 'GRD',
          'fpembayaran' => "transfer",
          'ftip' => 0,
          'fvoucher' => 0,
          'ftimein' => (isset($data['timein'])) ? $data['timein'] : '',
          'ftimeout' => (isset($data['timeout'])) ? $data['timeout'] : ''
        ];

        $data_detail = [[
          'fsono' => "APP.23.".sprintf("%04s", $data['id']),
          'fprdcode' => $code,
          'fqty' => 1,
          'fhpp' => $data["amount"],
          'fprice' => $data["amount"],
          'fpriceRp' => $data["amount"],
          'fdisc' => "0",
          'fpricenet' => $data['amount'],
          'fpricenetRp' => $data['amount'],
          'fpricenetRp' => $data['amount'],
          'famount' => $data['amount'],
          'famountRp' => $data['amount'],
          'fcode' => 'INV',
          'fshow' => '1'
        ]];
        $params = [
            'master'=>$data_master,
            'details' => $data_detail
        ];

        return $this->exec('POST', '/api/Sellings', $params, $token);
    }

    private function exec($method, $path, $data = [], $token='')
    {
        $headers = [
            'headers' => [
              'Content-Type' => 'application/json',
              'Content-Length' =>  strlen(Json::encode($data))
            ],
            'json' => $data
        ];

        if(!empty($token)){
            $headers['headers'] = [
                'Content-Type' => 'application/json',
                'Content-Length' =>  strlen(Json::encode($data)),
                'Authorization' => 'Bearer '.$token
            ];
        }

        $result = $this->http->request($method, $path, $headers);
        $body = (string) $result->getBody();
        $json = Json::decode($body);
        $code = (int) $result->getStatusCode();

        if($code != 200) {
            throw new GridsException($json->Message, $code);
        }

        return $json;
    }

    private function exec_form($method, $path, $data = [], $token='')
    {
        $headers = [
            'form_params' => $data
        ];
        if(!empty($token)){
            $headers['headers'] = [
                'Authorization' => 'Bearer '.$token
            ];
        }

        $result = $this->http->request($method, $path, $headers);
        $body = (string) $result->getBody();
        $json = Json::decode($body);
        $code = (int) $result->getStatusCode();

        if($code != 200) {
            throw new GridsException($json->Message, $code);
        }

        return $json;
    }

    protected function array_keys_exists(array $keys, array $arr)
    {
        return !array_diff_key(array_flip($keys), $arr);
    }
}

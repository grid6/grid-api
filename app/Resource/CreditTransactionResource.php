<?php

namespace App\Resource;

use Hyperf\Resource\Json\JsonResource;

class CreditTransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        // duitku channel
        // Credit Card => paymentUrl
        // Va Number => vaNumber
        // QRIS => qrString
        // OVO => paymentUrl
        // 
        return parent::toArray();
    }
}

<?php

namespace App\Resource;

use App\Model\PersonalBooked;
use Hyperf\Resource\Json\JsonResource;
use Qbhy\HyperfAuth\Annotation\Auth;
use Qbhy\HyperfAuth\AuthManager;

class PersonalPackageResource extends JsonResource
{
    // protected AuthManager $auth;
    /**
     * Transform the resource into an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        $totaljoined =  PersonalBooked::where(['personal'=>$this->personal, 'schedule_id'=>$this->id])
        ->count();
        if(!$totaljoined) $totaljoined = 0;
        if($totaljoined > $this->capacity) $totaljoined = $this->capacity;
        // $isbook = false;
        // if (isset($_SERVER['Authorization']) && !empty($_SERVER['Authorization'])) {
        //     $user = $this->auth->guard()->user();
        //     $bok = PersonalBooked::where([
        //         'personal'=>$this->personal,
        //         'schedule_id'=>$this->id,
        //         'user_id'=>$user->id
        //     ])->count();
        //     if($bok && $bok > 0) $isbook = true;
        // }

        return [
            'id' => (int) $this->id,
            'schedule_date' => (string) $this->schedule_date,
            'time_start' => (string) $this->time_start,
            'time_finish' => (string) $this->time_finish,
            'capacity' => (int) $this->capacity,
            'joined' => (int) $totaljoined
        ];
        // return parent::toArray();
    }
}

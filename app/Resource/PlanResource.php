<?php

namespace App\Resource;

use Hyperf\Resource\Json\JsonResource;

class PlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        // return parent::toArray();
        $ddesc = (string)$this->description;
        $desc = (!empty($ddesc)) ? explode('|', $ddesc) : [];

        return [
            "id" => (int)$this->id,
            "tag" => (string)$this->tag,
            "name" => (string)$this->name,
            "description" => (array)$desc,
            "is_active" => (bool)$this->is_active,
            "price" => (double)$this->price,
            "signup_fee" => (double)$this->signup_fee,
            "trial_period" => (int)$this->trial_period,
            "trial_interval" => (string)$this->trial_interval,
            "trial_mode" => (string)$this->trial_mode,
            "grace_period" => (int)$this->grace_period,
            "grace_interval" => (string)$this->grace_interval,
            "invoice_period" => (int)$this->invoice_period,
            "invoice_interval" => (string)$this->invoice_interval,
            "tier" => (int)$this->tier,
            "created_at"=> (string)$this->created_at
        ];
    }
}

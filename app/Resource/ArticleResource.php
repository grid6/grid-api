<?php

namespace App\Resource;

use Hyperf\Resource\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => (int) $this->id,
            'slug' => (string) $this->slug,
            'title' => (string) $this->title,
            'tags' => (array) $this->tags()->pluck('name')->toArray(),
            'cover' => (string) $this->cover,
            'cover_wide' => (string) $this->cover_wide,
            'content' => (string) $this->content,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}

<?php

namespace App\Resource;

use Hyperf\Resource\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array
     */
    public function toArray(): array
    {

        $userdatas =  [
            'id' => (int) $this->id,
            'first_name' => (string) $this->first_name,
            'last_name' => (string) $this->last_name,
            'email' => (string) $this->email,
            'phone' => (string) $this->phone,
            'gender' => (string) $this->gender,
            'photo' => (string) $this->photo,
            'address' => (string) $this->address,
            'type' => (int) $this->type,
            'verified' => (bool) ! is_null($this->email_verified_at),
            'created_at' => (string) $this->created_at
        ];
        if(isset($this->is_membership)) $userdatas['is_membership'] = (bool)$this->is_membership;
        return $userdatas;
    }
}

<?php

namespace App\Resource;

use Hyperf\Resource\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        // return parent::toArray();
        return [
            'id' => (string) $this->id,
            'activity' => (string) $this->activity,
            'data' => (array) $this->data,
            'read_at' => (string) $this->read_at,
            'created_at' => (string) $this->created_at
        ];
    }
}

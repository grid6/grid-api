<?php

namespace App\Resource;

use App\Model\ClassRoomJoined;
use Hyperf\Resource\Json\JsonResource;

class ActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        // return parent::toArray();
        $output =  [
            'id' => (int) $this->id,
            'class_id' => (int) $this->class_id,
            'schedule_date' => (string) $this->schedule_date,
            'time_start' => (string) $this->time_start,
            'time_finish' => (string) $this->time_finish,
            'time_finish' => (string) $this->time_finish,
            'trainer' => (int) $this->trainer,
            'capacity' => (int) $this->capacity,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at
        ];

        if (isset($this->class)){
            $output['class'] = $this->class;
        }

        if (isset($this->is_booked)){
            $output['is_booked'] = $this->is_booked;
        }

        $whr = [
            'schedule_id' => $this->id
        ];
        $tjoined = ClassRoomJoined::where($whr)->count();
        if(isset($this->pivot)){
            $output['pivot'] = $this->pivot;
            $whr['user_id'] = $this->pivot['user_id'];
        }
        $cjoined = ClassRoomJoined::select('is_waiting_list','attended_at')
        ->where($whr)
        ->first();
        $realcapacity = ($tjoined >= $this->capacity) ? $this->capacity : ($this->capacity - $tjoined);
        $output['capacity'] = (int)$realcapacity;
        $output['total_join'] = (int)$tjoined;
        $output['full_booked'] = ($tjoined >= $this->capacity) ? true : false;
        if (isset($whr['user_id'])) {
            if (!empty($cjoined->attended_at)) {
                  $output['status_attendance'] = 'present';
            } else {
                  $status_present = ($this->schedule_date < date("Y-m-d")) ? 'not presented' : 'unattended';
                  $output['status_attendance'] = $status_present;
            }
        }
        return $output;
    }
}

<?php

declare(strict_types=1);

namespace App\Listener;

use App\Event\PaymentSuccess;
use Hyperf\DbConnection\Db;
use App\Model\Trainer;
use App\Model\CreditPackage;
use App\Model\TrainerBooked;
use App\Model\TrainerPackage;
use App\Model\PersonalBooked;
use App\Model\Notification;
use App\Mail\TopupReceipt;
use App\Mail\PurchaseCourse;
use App\Mail\PurchaseSubscribe;
use App\Mail\PurchaseTrainer;
use Xtwoend\HySubscribe\Model\Plan;
use App\Pos\Grids;
use App\Xmqtt\Mqtt;
use App\Notification\Fcm;
use HyperfExt\Mail\Mail;
use Hyperf\Event\Annotation\Listener;
use Psr\Container\ContainerInterface;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
#[Listener]
class PaymentSuccessListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            PaymentSuccess::class
        ];
    }

    public function process(object $event)
    {
        if($event instanceof PaymentSuccess) {
            $transaction = $event->transaction;
            $user = $transaction->user;
            $usname = $user->first_name." ".$user->last_name;
            // deposit
            if(! $transaction->callback_processed && $transaction->status == 1) {
                $transaction->update(['callback_processed' => true]);
                $invoice_type = substr($transaction->invoice_no, 0, 2);
                // $transaction->update(['callback_processed' => true, 'callback_response' => $invoice_type]);
                $title = $body = "";
                $isadmq = false;
                $act = "purchase-success";
                if($invoice_type == "CR"){
                    $isadmq = true;
                    $transaction->update(['callback_response'=>$transaction->product]);
                    $credits = isset($transaction->product['credit']) ? $transaction->product['credit'] : 1;
                    $user->wallet()->update(['removed'=>'0', 'expired_at'=>date('Y-m-d H:i:s', strtotime('+30 days'))]);
                    $user->deposit($credits, [
                         'reference_no' => $transaction->reference_no,
                         'invoice_no' => $transaction->invoice_no,
                         'description' => 'Top-up '.$credits.' Credit(s)'
                     ]);
                     $prn = $transaction->product['name'];
                     $title = $usname." Successfully Purchase of ".$prn;
                     $body = "you successfully purchase ".$prn;
                     Mail::to($user->email)->send(new TopupReceipt($user, $transaction));
                }else if($invoice_type == "TB"){
                    $transaction->update(['callback_response'=>$transaction->product]);
                    $product = $transaction->product;
                    if(isset($product['id'])){
                        $daproduct = [
                          'trainer' => $product['id'],
                          'sessions' => $product['sessions'],
                          'trainer_level_sessions' => $product['level_sessions']['id'],
                          'user_id' => $user->id
                        ];
                        $sessions = (!empty($product['sessions'])) ? $product['sessions'] : $product['level_sessions']['sessions'];
                        $trpkg = TrainerPackage::where('sessions', $sessions)->first();
                        $expr = "";
                        if(isset($trpkg->durations)){
                          $expr = date("Y-m-d H:i:s", strtotime('+'.$trpkg->durations.' days'));
                          TrainerBooked::create([
                              'trainer'=>$product['id'],
                              'sessions'=>$product['sessions'],
                              'user_id'=>$user->id,
                              'expired_at'=>$expr
                          ]);
                        }
                        $prn = "Class ".$product['name']." - ".$product['sessions'];
                        $title = $usname." Successfully Purchase of ".$prn;
                        $body = "you successfully purchase ".$prn;
                        Mail::to($user->email)->send(new PurchaseTrainer($user, $transaction));
                    }
                }
                else if($invoice_type == "PD"){
                    $product = $transaction->product;
                    if(isset($product['id'])){
                        $daproduct = [
                          'personal' => $product['id'],
                          'sessions' => $product['sessions'],
                          'user_id' => $user->id
                        ];
                        PersonalBooked::create($daproduct);
                    }
                }
                else if($invoice_type == "SB"){
                    if(isset($transaction->product['id'])){
                        $isadmq = true;
                        $transaction->update(['callback_response'=>$transaction->product]);
                        $plan = Plan::find($transaction->product['id']);
                        $user->newSubscription('main', $plan, $plan->name, $plan->description);
                        Mail::to($user->email)->send(new PurchaseSubscribe($user, $transaction));
                        $prn = ucwords($transaction->product['tag'])." ".$transaction->product['name'];
                        $title = $usname." Successfully Purchase of ".$prn;
                        $body = "you successfully purchase ".$prn;
                    }
                }

                if(!empty($title) && !empty($body)){
                    $notif = [
                        'title' =>$title,
                        'body' =>$body,
                        'alert' => $title,
                        'icon' => 'https://placekitten.com/100/100',
                        'sound' => "default",
                    ];
                    $data_notif = [
                        'title'=>$title,
                        'body'=>$body,
                        'icon' => 'https://placekitten.com/100/100',
                        'priority' => 'high',
                        'content_available' => true
                    ];
                    $notifs = (new Fcm)->toTopic('user_'.$user->id)
                    ->notification($notif)
                    ->data($data_notif)
                    ->send();

                    $sel = "SELECT UUID() AS uuid_string";
                    Notification::create([
                        'id' =>  Db::select($sel)[0]->uuid_string,
                        'user_id' => $user->id,
                        'activity' => $act,
                        'data' => $transaction->toArray(),
                        'requests' => ['notif'=>$notif, 'data'=>$data_notif],
                        'responses'=> $notifs
                    ]);

                }

                $payment = (new Grids)->createTrans($transaction->toArray());
                if(!empty($user->face_registered) && $isadmq){
                    $device = (new Mqtt)->devices();
                    if(isset($device['data'])){
                        $respz = [];
                        foreach($device['data'] as $dvc){
                          $paramq = [
                            'id'=>$user->id,
                            'device'=>$dvc['id'],
                            'first_name' => $user->first_name,
                            'last_name' => (string)$user->last_name,
                            'face_registered'=>$user->face_registered
                          ];
                          $respz = (new Mqtt)->enroll($paramq);
                        }
                    }
                }
            }
        }
    }
}

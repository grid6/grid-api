<?php

declare(strict_types=1);

namespace App\Listener;

use HyperfExt\Mail\Mail;
use App\Event\Registered;
use App\Mail\VerifyEmail;
use Hyperf\Event\Annotation\Listener;
use Psr\Container\ContainerInterface;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
#[Listener]
class RegisteredListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
            Registered::class
        ];
    }

    public function process(object $event)
    {
        if($event instanceof Registered) {
            $user = $event->user;
            Mail::to($user->email)->send(new VerifyEmail($user));
        }
    }
}

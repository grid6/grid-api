<?php

declare(strict_types=1);

namespace App\Listener;

use Hyperf\Event\Annotation\Listener;
use App\Event\BookClassSuccess;
use App\Model\User;
use App\Mail\BookReminder;
use Psr\Container\ContainerInterface;
use Hyperf\Event\Contract\ListenerInterface;

/**
 * @Listener
 */
#[Listener]
class BookClassSuccessListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function listen(): array
    {
        return [
          BookClassSuccess::class
        ];
    }

    public function process(object $event)
    {
        if($event instanceof BookClassSuccess) {
            $course = $event->course;
            $user = User::find($course->user_id);

            Mail::to($user->email)->send(new BookReminder($user, $course));
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use App\Payment\Duitku;
use App\Notification\Fcm;
use App\Model\User;
use App\Model\ClassRoom;
use App\Model\Category;
use App\Model\Personal;
use App\Model\PersonalPackage;
use App\Model\PersonalBooked;
use App\Model\ClassRoomJoined;
use App\Model\ClassRoomSchedule;
use App\Model\Notification;
use HyperfExt\Mail\Mail;
use App\Mail\BookReminder;
use App\Mail\CancelBookClass;
use App\Event\BookClassSuccess;
use App\Constants\ErrorCode;
use App\Resource\ClassResource;
use App\Resource\PersonalResource;
use App\Resource\PersonalBookedResource;
use App\Resource\PersonalPackageResource;
use App\Resource\CategoryResource;
use App\Resource\PaymentTransactionResource;
use Hyperf\DbConnection\Db;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use App\Model\PaymentTransaction;
use App\Resource\ActivityResource;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;

#[Controller]
class ClassController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected EventDispatcherInterface $event;

    #[RequestMapping(path: "/api/classes", methods: "get")]
    public function index(RequestInterface $request)
    {
        $rpp = $request->input('per_page', 6);
        $rows = ClassRoom::select('*')->active();

        if($request->has('category_id')) {
            $rows = $rows->where('category_id', $request->input('category_id'));
        }

        $rows = $rows->paginate((int)$rpp);

        return response(ClassResource::collection($rows));
    }

    #[RequestMapping(path: "/api/classes/{id}", methods: "get")]
    public function detail($id, RequestInterface $request)
    {
        $row = ClassRoom::with('schedules')->find($id);
        if(!$row->id){
            return response('data class not found', ErrorCode::DATA_NOTFOUND);
        }

        if (!empty($request->header('Authorization'))) {
          $user = $this->auth->guard()->user();
          if(!empty($row->schedules)){
              $schedd = [];
              foreach($row->schedules as $sched){
                $total_join = 0;
                $is_book = false;
                $qr = "";
                if($sched->schedule_date >= date("Y-m-d")){
                  $whr1 = [
                      'class_id'=>$id,
                      'schedule_id'=>$sched->id
                  ];
                  $tjoin = ClassRoomJoined::where($whr1)
                  ->count();
                  if($tjoin && $tjoin > 0){
                      $total_join = $tjoin;
                      if($tjoin > $sched->capacity){
                        $total_join = $sched->capacity;
                        $sched->full_booked = true;
                      }
                  }
                  $whr1['user_id'] = $user->id;
                  $booked = ClassRoomJoined::where($whr1)
                  ->first();
                  $is_book = (isset($booked->id)) ? true : false;
                  $sched->capacity = $sched->capacity - $total_join;
                  if($is_book !== false){
                    $qr = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=";
                    $qr.= $id."-".$sched->id."-".$user->id;
                    $qr.= "&choe=UTF-8";
                  }
                }
                $sched->total_join = $total_join;
                $sched->is_book = $is_book;
                $sched->qr = $qr;
                $schedd[] = $sched;
              }
              $row->schedules = $schedd;
          }

        }

        return response(new ClassResource($row));
    }

    #[RequestMapping(path: "/api/classes/{id}/cancel", methods: "post")]
    public function cancel($id, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'schedule_id' => 'required|exists:classes_schedules,id',
            'password' => 'required|min:6'
        ])->validate();

        $class = ClassRoom::find($id);
        if(!$class) return response('class not found', 404);

        if(! password_verify($request->input('password'), $user->password)) {
            return response('Invalid password.', ErrorCode::FORM_ERROR);
        }

        $schedule = ClassRoomSchedule::find($request->input('schedule_id'));
        $room  = $schedule->class;

        $whr =[
            'schedule_id'=>$schedule->id,
            'class_id'=>$id,
            'user_id'=>$user->id
        ];

        $joined = ClassRoomJoined::where($whr)->first();
        $date1 = strtotime($joined->schedule_date);
        $date2 = strtotime("now");
        $diff = $date2 - $date1;
        $hours = round($diff / 3600);
        if(!isset($joined->id)){
            return response("this user not join this class", 400);
        }else if(!empty($joined->attended_at)){
            return response("this user already attend this class", 400);
        }else if(date("Y-m-d", strtotime($joined->schedule_date)) < date("Y-m-d")){
            return response("cancel failed, class already started", ErrorCode::FORM_ERROR);
        }else if($hours >= 1){
            return response("minimum cancel is 1 hour before start", ErrorCode::FORM_ERROR);
        }

        $joned = (is_object($joined)) ? $joined->toArray() : [];

        $memberz = false;
        if($user->type != 2){
            $plan = PlanSubscription::where(['user_id'=>$user->id])->first();
            if(isset($plan->id)){
                $memberz=true;
                // return response('this user isnt membership');
            }
        }else $memberz = true;

        $cprice = 0;
        if(!$memberz){
            $cprice = (int)$class->price;
            $joned["title"] = "Refund Class";
            $user->deposit($cprice, $joned);
        }

        $joined->delete();

        Mail::to($user->email)->send(new CancelBookClass($user, $class, $cprice));
        $classn = $class->name;
        $sdate = $schedule->schedule_date;
        $title = 'Hi '.$user->first_name.', you are cancel booking of '.$classn;
        $body = 'you cancel book this class '.$classn.' at '.$sdate;
        $notif = [
            'title' =>$title,
            'body' =>$body,
            'alert' => $title,
            'icon' => 'https://placekitten.com/100/100',
            'sound' => "default",
        ];
        $data_notif = [
            'title'=>$title,
            'body'=>$body,
            'icon' => 'https://placekitten.com/100/100',
            'priority' => 'high',
            'content_available' => true
        ];
        $notifs = (new Fcm)->toTopic('user_'.$user->id)
        ->notification($notif)
        ->data($data_notif)
        ->send();

        $data = [
          'title' => $title,
          'message' => $body,
          'data' => $joned
        ];
        Notification::create([
          'id' =>  Db::select("SELECT UUID() AS uuid_string")[0]->uuid_string,
          'user_id' => $user->id,
          'activity' => "class-cancel",
          'data' => json_encode($data),
          'requests' => ['notif'=>$notif, 'data'=>$data_notif],
          'responses'=> $notifs
        ]);

        return response("you successfully cancel this class", 0);
    }

    #[RequestMapping(path: "/api/classes/{id}/schedules", methods: "post")]
    public function schedules($id, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();
        $date = $request->input('date', Carbon::now()->format('Y-m-d'));

        $month = Carbon::parse($date);

        $is_booked = false;
        $total_join = $booked_sess = 0;

        $booked = ClassRoomJoined::select('schedule_date','sessions', 'created_at')
        ->where(['schedule_id'=>$id, 'class_id'=>$id])
        ->count();

        if($booked > 0){
            $booked_sess = (int)$booked->sessions;
            $whjo = [
              'user_id'=>$user->id,
              'class_id'=>$id,
              'schedule_id'=>$id
            ];
            $jodata = ClassRoomJoined::where($whjo)
            ->first();
            if($jodata && isset($jodata->id)) $jodata = $booked->sessions;
            $total_join = ($jodata) ? (int)$jodata : 0;
            $is_booked = true;
        }

        $activities = ClassRoomSchedule::where("class_id", $id)
            ->with('class')
            ->whereDate('schedule_date', '=', $month->format('Y-m-d'))
            ->get();

        return response(ActivityResource::collection($activities));
    }

    #[RequestMapping(path: "/api/class/joined", methods: "get")]
    public function joined(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();
        $date = date("Y-m-d");
        $month = Carbon::parse($date);

        $rpp = $request->input('per_page', 6);

        $rows = $user->schedules()
            ->with('class')
            ->where('classes_schedules.schedule_date', '>=', $date)
            ->get();

        // $rows = $rows->paginate($rpp);
        return response(ActivityResource::collection($rows));

        // return response(ActivityResource::collection($rows) );
    }

    #[RequestMapping(path: "/api/classes/join", methods: "post")]
    public function join(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'class_id' => 'required|exists:classes,id',
            'schedule_id' => 'required|exists:classes_schedules,id',
            'password' => 'required|min:6'
        ])->validate();

        if(! password_verify($request->input('password'), $user->password)) {
            return response('Invalid password.', ErrorCode::FORM_ERROR);
        }

        $schedule = ClassRoomSchedule::find($request->input('schedule_id'));
        $room  = $schedule->class;


        $whr =[
          'schedule_id'=>$request->input('schedule_id'),
          'class_id'=>$request->input('class_id'),
          'user_id'=>$user->id
        ];

        $joined = ClassRoomJoined::where($whr)->first();
        if(isset($joined->id)){
            return response("user already join this class", 400);
        }else if($schedule->schedule_date->format("Y-m-d") < date("Y-m-d")){
            return response("you can't join old class", 401);
        }

        //checker type sesuai
        $rprice = 0;
        if(! $user->subscriptionActive()) {
            // throw new InsufficientFunds();
              if($user->balance < $room->price){
                return response("insufficient balance", 401);
              }

              $rprice = $room->price;
              // $user->withdraw($room->price, [
              //     'class_id' => $request->input('class_id'),
              //     'session_id' => $request->input('schedule_id'),
              //     'description' => 'book class success'
              // ]);
        }

        $user->withdraw($rprice, [
            'class_id' => $request->input('class_id'),
            'schedule_id' => $request->input('schedule_id'),
            'description' => 'Booking Class'
        ]);

        $joinned = $user->joinClass($schedule);

        $whc = [
          'user_id'=>$user->id,
          'schedule_id'=>$schedule->id,
          'class_id'=>$request->input('class_id')
        ];

        $cj = ClassRoomJoined::where($whc)
        ->with('class')
        ->with('schedule')
        ->first();
        if(isset($cj->id)){
            Mail::to($user->email)->send(new BookReminder($user, $cj));
            $usname = $user->first_name.' '.$user->last_name;
            $classname = $cj->class->name;
            $sdate = $schedule->schedule_date;
            $title = $usname.', Success Booking of '.$classname;
            $body = 'you have book class '.$classname.' at '.$sdate;

            $notif = [
                'title' =>$title,
                'body' =>$body,
                'alert' => $title,
                'icon' => 'https://placekitten.com/100/100',
                'sound' => "default",
            ];
            $data_notif = [
                'title'=>$title,
                'body'=>$body,
                'icon' => 'https://placekitten.com/100/100',
                'priority' => 'high',
                'content_available' => true
            ];
            $notifs = (new Fcm)->toTopic('user_'.$user->id)
            ->notification($notif)
            ->data($data_notif)
            ->send();
            $cj->data_notif = $notifs;
            $data = [
              'title' => $title,
              'message' => $body,
              'data' => $cj
            ];
            Notification::create([
              'id' =>  Db::select("SELECT UUID() AS uuid_string")[0]->uuid_string,
              'user_id' => $user->id,
              'activity' => "class-booking",
              'data' => json_encode($data),
              'requests' => ['notif'=>$notif, 'data'=>$data_notif],
              'responses'=> $notifs
            ]);
        }
        return response($cj, 0);
    }

    #[RequestMapping(path: "/api/category", methods: "get")]
    public function category(RequestInterface $request)
    {
        $rows = Category::all();

        return response(CategoryResource::collection($rows));
    }

    #[RequestMapping(path: "/api/category/{category}", methods: "get")]
    public function classOnCategory($category, RequestInterface $request)
    {
        $rpp = $request->input('per_page', 6);
        $category = Category::whereSlug($category)->first();
        if(!isset($category->id)){
          return response("Category not found", ErrorCode::DATA_NOTFOUND);
        }

        $rows = $category->classes()->active()->paginate($rpp);

        return response(ClassResource::collection($rows));
    }

    #[RequestMapping(path: "/api/classes/attend", methods: "post")]
    public function attend(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'schedule_id' => 'required'
        ])->validate();

        $schedule = ClassRoomJoined::where([
            'user_id' => $user->id,
            'schedule_id' => $request->input('schedule_id')
        ])->first();

        if(!$schedule->schedule_id){
          return response("Schedule not found", 404);
        }

        $schedule->forceFill([
            'attended_at' => Carbon::now()->format('Y-m-d H:i:s')
        ])->save();

        return response($schedule, 0);
    }

    #[RequestMapping(path: "/api/classes/activites", methods: "post")]
    public function activity(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();
        $date = $request->input('date', Carbon::now()->format('Y-m-d'));

        $month = Carbon::parse($date);

        $activities = $user->schedules()
            ->with('class')
            ->whereYear('classes_schedules.schedule_date', '=', $month->format('Y'))
            ->whereMonth('classes_schedules.schedule_date', '=', $month->format('m'))
            ->get();

        return response(ActivityResource::collection($activities));
    }

    #[RequestMapping(path: "/api/classes/reservation", methods: "post")]
    public function reservation(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'class_id' => 'required|exists:classes,id',
            'schedule_id' => 'required|exists:classes_schedules,id',
            'channel' => 'required'
        ])->validate();

        $schedule = ClassRoomSchedule::find($request->input('schedule_id'));
        $room  = ClassRoom::find($schedule->class_id);

        if($room->vt_class) {
            return response("Can't reservation for this class", 403);
        }

        $amount = $room->price_special > 0 ? $room->price_special: $room->price;

        if($room->discount > 0) {
            $amount = $room->price - (($room->price * $room->discount) / 100);
        }

        $trans = PaymentTransaction::create([
            'user_id' => $user->id,
            'payable_id' => $room->id,
            'payable_type' => (is_object($room)) ? get_class($room) : "App\Model\ClassRoomSchedule",
            'status'  => 0,
            'channel' => $request->input('channel'),
            'amount' => $amount,
            'expire_in' => Carbon::now()->addMinutes(config('pga.duitku.expire_in')),
            'product' => (is_object($room)) ? $room->toArray() : []
        ]);

        $format = str_pad((string) $trans->id, 6, '0', STR_PAD_LEFT);
        $invoice = "CR/{$trans->created_at->format('Ymd')}/{$format}";
        $trans->invoice_no = $invoice;
        $trans->save();

        $payment = (new Duitku)->create([
            'invoice_no' => $trans->invoice_no,
            'channel' => $trans->channel,
            'amount' => $trans->amount,
            'description' => $room->name,
            'user' => [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phoneFormat,
                'address' => $user->address
            ],
            'items' => [
                [
                    'name' => $room->name,
                    'price' => $amount,
                    'quantity' => 1
                ]
            ]
        ]);

        $trans->update(['payment_create' => $payment, 'reference_no' => $payment['reference']]);

        return response(new PaymentTransactionResource($trans));
    }

    #[RequestMapping(path: "/public/{usr}/classes/{courses}/join/{sched}", methods: "get")]
    public function pjoin($usr, $courses, $sched, RequestInterface $request)
    {
        $user = User::find($usr);
        if(!$user){
            return response('user not found', 404);
        }

        $class = ClassRoom::find($courses);
        if(!isset($class->id)){
            return response("class not exist", 404);
        }
        $room  = $class;

        $schedule = ClassRoomSchedule::find($sched);
        if(!isset($schedule->id)){
            return response('schedule not found', 404);
        }

        $whr =[
          'schedule_id'=>$sched,
          'class_id'=>$courses,
          'user_id'=>$user->id
        ];

        $joined = ClassRoomJoined::where($whr)->first();
        if(isset($joined->id)){
            return response("user already join this class", 400);
        }else if($schedule->schedule_date->format("Y-m-d") < date("Y-m-d")){
            return response("you can't join old class", 401);
        }

        //checker type sesuai
        $rprice = 0;
        if(! $user->subscriptionActive()) {
            // throw new InsufficientFunds();
              if($user->balance < $room->price){
                return response("insufficient balance", 401);
              }

              $rprice = $room->price;
              // $user->withdraw($room->price, [
              //     'class_id' => $request->input('class_id'),
              //     'session_id' => $request->input('schedule_id'),
              //     'description' => 'book class success'
              // ]);
        }

        $user->withdraw($rprice, [
            'class_id' => $courses,
            'schedule_id' => $schedule->id,
            'description' => 'Booking Class'
        ]);

        $joinned = $user->joinClass($schedule);

        $whc = [
            'user_id'=>$user->id,
            'schedule_id'=>$schedule->id,
            'class_id'=>$courses
        ];

        $cj = ClassRoomJoined::where($whc)
        ->with('class')
        ->with('schedule')
        ->first();
        if(isset($cj->id)){
            Mail::to($user->email)->send(new BookReminder($user, $cj));
            $usname = $user->first_name.' '.$user->last_name;
            $classname = $cj->class->name;
            $sdate = $schedule->schedule_date;
            $title = $usname.', Success Booking of '.$classname;
            $body = 'you have book class '.$classname.' at '.$sdate;

            $notif = [
                'title' =>$title,
                'body' =>$body,
                'alert' => $title,
                'icon' => 'https://placekitten.com/100/100',
                'sound' => "default",
            ];
            $data_notif = [
                'title'=>$title,
                'body'=>$body,
                'icon' => 'https://placekitten.com/100/100',
                'priority' => 'high',
                'content_available' => true
            ];
            $notifs = (new Fcm)->toTopic('user_'.$user->id)
            ->notification($notif)
            ->data($data_notif)
            ->send();
            $cj->data_notif = $notifs;
            $data = [
              'title' => $title,
              'message' => $body,
              'data' => $cj
            ];
            Notification::create([
              'id' =>  Db::select("SELECT UUID() AS uuid_string")[0]->uuid_string,
              'user_id' => $user->id,
              'activity' => "class-booking",
              'data' => json_encode($data),
              'requests' => ['notif'=>$notif, 'data'=>$data_notif],
              'responses'=> $notifs
            ]);
        }
        return response($cj, 0);
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use App\Model\Notification;
use App\Constants\ErrorCode;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use App\Resource\ActivityResource;
use App\Resource\NotificationResource;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;


#[Controller]
class NotifController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected EventDispatcherInterface $event;

    #[RequestMapping(path: "/api/notif", methods: "get")]
    public function lists(RequestInterface $request)
    {

        $user = $this->auth->guard()->user();

        $rows = Notification::where(['user_id'=>$user->id])->get();
        if(empty($rows) || !$rows){
            return response('notif still empty', 0);
        }

        return response(NotificationResource::collection($rows));
    }

    #[RequestMapping(path: "/api/notif/{id}", methods: "get")]
    public function detail($id, RequestInterface $request)
    {

        $user = $this->auth->guard()->user();

        $rows = Notification::where(['id'=>$id, 'user_id'=>$user->id])->first();
        if(!$rows->id){
            return response('data notification not found', 404);
        }

        return response(new NotificationResource($rows));
    }
}

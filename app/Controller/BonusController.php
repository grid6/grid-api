<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use App\Model\Bonus;
use App\Model\TrainerSchedule;
use App\Constants\ErrorCode;
use App\Resource\BonusResource;
use App\Resource\ActivityResource;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;


#[Controller]
class BonusController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected EventDispatcherInterface $event;

    #[RequestMapping(path: "/api/bonus", methods: "get")]
    public function index(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $bonus = Bonus::select('amount')->where(['user_id'=>$user->id, 'is_claimed'=>0])->first();

        return response($bonus);
    }

    #[RequestMapping(path: "/api/bonus/claim", methods: "put")]
    public function claim(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $bonus = Bonus::where(['user_id'=>$user->id, 'is_claimed'=>0])->first();
        if(!$bonus){
            return response('there is no bonus to claim', 404);
        }

        $user->deposit($bonus->amount, [
            'claim_at' => date('Y-m-d H:i:s'),
            'description' => 'Claim bonus'
        ]);

        $bonus->is_claimed = 1;
        $bonus->save();

        return response('claim bonus success', 0);
    }

    #[RequestMapping(path: "/api/me/activities", methods: "post")]
    public function activities(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();
        $date = $request->input('date', Carbon::now()->format('Y-m-d'));

        $month = Carbon::parse($date);

        $activities = $user->schedules()
            ->with('class')
            ->whereYear('classes_schedules.schedule_date', '=', $month->format('Y'))
            ->whereMonth('classes_schedules.schedule_date', '=', $month->format('m'))
            ->get();

        $activities2 = $user->tschedules()
            ->with('trainer')
            ->whereYear('trainer_schedules.schedule_date', '=', $month->format('Y'))
            ->whereMonth('trainer_schedules.schedule_date', '=', $month->format('m'))
            ->get();

       $listact = [
          'trainer' => $activities2,
          'class'=>ActivityResource::collection($activities)
       ];
        return response($listact);
    }
}

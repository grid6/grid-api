<?php

declare(strict_types=1);

namespace App\Controller;

use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use App\Model\PaymentTransaction;
use App\Resource\TransactionResource;
use App\Resource\PaymentTransactionResource;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;

#[Controller]
class WalletController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[RequestMapping(path: "/api/wallet", methods: "get")]
    public function index(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        return response([
            'balance' => (float) $user->balance
        ]);
    }

    #[RequestMapping(path: "/api/wallet/transaction", methods: "get")]
    public function histories(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();
        $rpp = $request->input('per_page', 12);

        $transactions = $user->transactions()->latest()->paginate($rpp);

        return response(TransactionResource::collection($transactions));
    }

    #[RequestMapping(path: "/api/payment/transaction", methods: "get")]
    public function phistories(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();
        $rpp = $request->input('per_page', 12);

        $whr = ['user_id' => $user->id];
        if(!empty($request->input("status"))){
            $stat = $request->input("status");
            if($stat == 0 || $stat == "pending"){
                $whr['status'] = 0;
            }else if($stat == 1 || $stat == "success"){
                $whr['status'] = 1;
            }else if($stat == 2 || $stat == "failed"){
                $whr['status'] = 2;
            }
        }

        $transactions = PaymentTransaction::where($whr)->latest()->get();
        $transes = [];
        foreach($transactions as $tr){
          switch($tr->channel){
              case "BC" : $channel="Virtual Account BCA";break;
              case "BR" : $channel="Virtual Account BRI";break;
              case "NC" : $channel="Virtual Account BNC";break;
              case "NQ" : $channel="Virtual Account Nobu QRIS";break;
              case "VA" : $channel="Virtual Account Maybank";break;
              case "BT" : $channel="Virtual Account Permata";break;
              case "B1" : $channel="Virtual Account Cimb Niaga";break;
              case "A1" : $channel="Virtual Account ATM Bersama";break;
              case "I1" : $channel="Virtual Account BNI";break;
              case "M1" : $channel="Virtual Account Mandiri";break;
              case "M2" : $channel="Virtual Account Mandiri H2H";break;
              case "OV" : $channel="OVO";break;
              case "OL" : $channel="OVO Link";break;
              case "SP" : $channel="Shopeepay QRIS";break;
              case "SA" : $channel="Shopeepay Apps";break;
              case "SL" : $channel="Shopeepay Link";break;
              case "DN" : $channel="Indodana Paylater";break;
              case "LA" : $channel="LinkAja";break;
              case "VC" : $channel="Credit Card";break;
              case "FT" : $channel="Retail";break;
              case "IR" : $channel="Indomaret";break;
              default : $channel = "Virtual Account BCA";break;
          }

          $tr->payment_channel = $channel;

          if($tr->status == 0){
            $tr->payment_status = "pending";
          } else if($tr->status == 1){
            $tr->payment_status = "success";
          }else if($tr->status == 2){
            $tr->payment_status = "failed";
          }

          $transes[] = $tr;
        }

        return response(PaymentTransactionResource::collection($transes));
    }

    #[RequestMapping(path: "/api/payment/transaction/{id}", methods: "get")]
    public function phistdetail($id, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();
        $rpp = $request->input('per_page', 12);

        $transactions = PaymentTransaction::find($id);

        if(!isset($transactions->id)){
            return response("payment detail not found", 404);
        }

        switch($transactions->channel){
            case "BC" : $channel="Virtual Account BCA";break;
            case "BR" : $channel="Virtual Account BRI";break;
            case "NC" : $channel="Virtual Account BNC";break;
            case "NQ" : $channel="Virtual Account Nobu QRIS";break;
            case "VA" : $channel="Virtual Account Maybank";break;
            case "BT" : $channel="Virtual Account Permata";break;
            case "B1" : $channel="Virtual Account Cimb Niaga";break;
            case "A1" : $channel="Virtual Account ATM Bersama";break;
            case "I1" : $channel="Virtual Account BNI";break;
            case "M1" : $channel="Virtual Account Mandiri";break;
            case "M2" : $channel="Virtual Account Mandiri H2H";break;
            case "OV" : $channel="OVO";break;
            case "OL" : $channel="OVO Link";break;
            case "SP" : $channel="Shopeepay QRIS";break;
            case "SA" : $channel="Shopeepay Apps";break;
            case "SL" : $channel="Shopeepay Link";break;
            case "DN" : $channel="Indodana Paylater";break;
            case "LA" : $channel="LinkAja";break;
            case "VC" : $channel="Credit Card";break;
            case "FT" : $channel="Retail";break;
            case "IR" : $channel="Indomaret";break;
            default : $channel = "Virtual Account BCA";break;
        }
        $transactions->payment_channel = $channel;
        if($transactions->status == 0){
          $transactions->payment_status = "pending";
        } else if($transactions->status == 1){
          $transactions->payment_status = "success";
        }else if($transactions->status == 2){
          $transactions->payment_status = "failed";
        }

        return response(new PaymentTransactionResource($transactions));
    }

    #[RequestMapping(path: "/api/wallet/transaction/{uuid}", methods: "get")]
    public function detail($uuid, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $whr = [
          'uuid' => $uuid
        ];
        $transaction = $user->transactions()->where($whr)->first();
        if(!isset($transaction->id)){
            return response("Transaction not found", 404);
        }

        return response(new TransactionResource($transaction));
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use App\Notification\Fcm;
use App\Model\User;
use App\Model\Trainer;
use App\Model\TrainerBooked;
use App\Model\TrainerLevel;
use App\Model\TrainerPackage;
use App\Model\TrainerSchedule;
use App\Model\TrainerJoined;
use App\Model\TrainerLevelSession;
use App\Model\Notification;
use App\Model\Setting;
use App\Payment\Duitku;
use App\Event\PaymentSuccess;
use App\Constants\ErrorCode;
use HyperfExt\Mail\Mail;
use App\Mail\BookTrainer;
use App\Mail\CancelBookTrainer;
use App\Resource\TrainerResource;
use App\Resource\TrainerBookedResource;
use App\Resource\TrainerPackageResource;
use App\Resource\TrainerScheduleResource;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use App\Model\PaymentTransaction;
use App\Resource\PaymentTransactionResource;
use App\Resource\ActivityResource;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;

#[Controller]
class TrainerController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected EventDispatcherInterface $event;

    #[RequestMapping(path: "/api/personal-trainer", methods: "get")]
    public function personal(RequestInterface $request)
    {
        if (isset($_SERVER['Authorization']) && !empty($_SERVER['Authorization'])) {
            $user = $this->auth->guard()->user();
        }

        $search = $request->input("search");

        if(!empty($search)){
            $search = urldecode($search);
            $trainer = Trainer::with('level')
            ->with('category')
            ->where('name', 'LIKE', '%'.$search.'%')
            ->get();
        }else{
            $trainer = Trainer::where('level', '!=', 0)->with('level')->with('category')->get();
        }

        if(!$trainer){
            return response('data trainer is empty', 0);
        }

        return response(TrainerResource::collection($trainer));
    }

    #[RequestMapping(path: "/api/personal-training/booked", methods: "get")]
    public function listbooked(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $search = $request->input("search");

        if(!empty($search)){
            $search = urldecode($search);
            $trainer = Trainer::whereHas('booked', function ($query) use($user){
                return $query->where('user_id', '=', $user->id);
            })
            ->with('level')
            ->with('category')
            ->where('name', 'LIKE', '%'.$search.'%')
            ->get();
        }else{
            $trainer = Trainer::where('level', '!=', 0)
            ->whereHas('booked', function ($query) use($user){
                return $query->where('user_id', '=', $user->id);
            })
            ->with('level')
            ->with('category')
            ->get();
        }

        if(!$trainer){
            return response('data trainer is empty', 0);
        }

        foreach($trainer as $tr){
            $is_booked = false;
            $total_join = $booked_sess = 0;
            $booked = TrainerBooked::select('schedule_date','sessions', 'created_at')
            ->where(['user_id'=>$user->id, 'trainer'=>$tr->id])
            ->first();
            if(isset($booked->sessions)){
                $is_booked = true;
                $booked_sess = (int)$booked->sessions;
                $jodata = TrainerJoined::where(['user_id'=>$user->id, 'trainer_id'=>$tr->id])
                ->count();
                if($jodata && $jodata > $booked->sessions) $jodata = $booked->sessions;
                $total_join = ($jodata) ? (int)$jodata : 0;
            }
            $tr->is_booked = $is_booked;
            $tr->booked_session = $booked_sess;
            $tr->total_join = $total_join;
        }

        return response(TrainerResource::collection($trainer));
    }

    #[RequestMapping(path: "/api/personal-training/package", methods: "get")]
    public function package(RequestInterface $request)
    {
        $user = null;
        if (isset($_SERVER['Authorization']) && !empty($_SERVER['Authorization'])) {
            $user = $this->auth->guard()->user();
        }

        $setting = Setting::where(['key'=>'max_ncpt'])->first();
        if(!($setting->value)) return response('need to setup max_ncpt first', 400);
        $max_ncpt = $setting->value;

        if(isset($user->id)){
            $packages = TrainerPackage::where('sessions', '<=', $max_ncpt)
                        ->get();
        }else $packages = TrainerPackage::all();
        if(!$packages){
            return response('data package is empty', 0);
        }

        return response(TrainerPackageResource::collection($packages));
    }

    #[RequestMapping(path: "/api/personal-training/{id}/package", methods: "get")]
    public function pptrain($id, RequestInterface $request)
    {
        $is_booked = false;
        $total_join = $booked_sess = 0;
        $book_data = null;

        $max_ncpt = 0;
        $is_member = false;
        $setting = Setting::where(['key'=>'max_ncpt'])->first();
        if(!($setting->value)) return response('need to setup max_ncpt first', 400);
        $max_ncpt = $setting->value;

        if (!empty($request->header('Authorization'))) {
            $user = $this->auth->guard()->user();
            //checker penalty
            if($user->type != 2){
                $plan = PlanSubscription::where(['user_id'=>$user->id])
                ->where('ends_at', '>', date("Y-m-d H:i:s"))
                ->first();
                if(isset($plan->id)) $is_member = true;
            }else $is_member = true;

            $booked = TrainerBooked::select('schedule_date','sessions', 'created_at')
            ->where(['user_id'=>$user->id, 'trainer'=>$id])
            ->first();
            if(isset($booked->sessions)){
                $is_booked = true;
                $book_data = $booked;
                $booked_sess = (int)$booked->sessions;
                $jodata = TrainerJoined::where(['user_id'=>$user->id, 'trainer_id'=>$id])
                ->count();
                if($jodata && $jodata > $booked->sessions) $jodata = $booked->sessions;
                $total_join = ($jodata) ? (int)$jodata : 0;
            }
        }

        $trainer = Trainer::with('level')->with('category')->find($id);
        if(!$trainer) response('trainer not found', 404);

        $trainer->is_book = $is_booked;
        $trainer->booked_session = $booked_sess;
        $trainer->total_join = $total_join;
        $trainer->book_data = $booked;

        if(!$is_member){
          $packages = TrainerLevelSession::select('id', 'level_id', 'sessions', 'price')
          ->where(['level_id'=>$trainer->level])
          ->where('sessions', '<=', $max_ncpt)
          ->with('level')
          ->get();
        }else{
          $packages = TrainerLevelSession::select('id', 'level_id', 'sessions', 'price')
          ->where(['level_id'=>$trainer->level])
          ->with('level')
          ->get();
        }
        if(!$packages){
            return response('data package is empty', 0);
        }

        $trainer->packages = $packages;

        return response($trainer, 0);
    }

    #[RequestMapping(path: "/api/personal-trainer/{id}", methods: "get")]
    public function detail($id, RequestInterface $request)
    {
        $trainer = Trainer::with('level')->with('category')->find($id);
        if(!$trainer){
            return response('trainer not found', 404);
        }

        $is_booked = false;
        $total_join = $booked_sess = 0;
        $book_data = null;

        if (!empty($request->header('Authorization'))) {
            $user = $this->auth->guard()->user();
            $booked = TrainerBooked::select('schedule_date','sessions', 'created_at')
            ->where(['user_id'=>$user->id, 'trainer'=>$id])
            ->first();
            if(isset($booked->sessions)){
                $is_booked = true;
                $book_data = $booked;
                $booked_sess = (int)$booked->sessions;
                $jodata = TrainerJoined::where(['user_id'=>$user->id, 'trainer_id'=>$id])
                ->count();
                if($jodata && $jodata > $booked->sessions) $jodata = $booked->sessions;
                $total_join = ($jodata) ? (int)$jodata : 0;
            }
        }

        $trainer->is_book = $is_booked;
        $trainer->booked_session = $booked_sess;
        $trainer->total_join = $total_join;
        $trainer->book_data = $booked;

        return response($trainer, 0);
    }

    #[RequestMapping(path: "/api/personal-trainer/{id}/schedule", methods: "post")]
    public function schedule($id, RequestInterface $request)
    {

        $user = $this->auth->guard()->user();

        $is_booked = false;
        $total_join = $booked_sess = 0;

        $booked = TrainerBooked::select('schedule_date','sessions', 'created_at')
        ->where(['user_id'=>$user->id, 'trainer'=>$id])
        ->first();

        if(isset($booked->sessions)){
            $is_booked = true;
            $booked_sess = (int)$booked->sessions;
            $jodata = TrainerJoined::where(['user_id'=>$user->id, 'trainer_id'=>$id])
            ->count();
            if($jodata && $jodata > $booked->sessions) $jodata = $booked->sessions;
            $total_join = ($jodata) ? (int)$jodata : 0;
        }

        $this->validator->make($request->all(), [
            'date' => 'required'
        ])->validate();

        $date = $request->input('date', Carbon::now()->format('Y-m-d'));
        $month = Carbon::parse($date);

        $trainer = Trainer::with('level')->with('category')->find($id);
        if(!$trainer) return response("trainer not found", 404);

        $trainer->is_book = $is_booked;
        $trainer->booked_session = $booked_sess;
        $trainer->total_join = $total_join;

        $packages = TrainerSchedule::where('trainer_id', $id)
        ->whereDate('schedule_date', '=', $month->format('Y-m-d'))
        ->with('trainer')
        ->get();

        if(!$packages) return response('personal development schedule is empty', 0);

        $trainers = [
          'trainer' => $trainer,
          'schedule' => TrainerScheduleResource::collection($packages)
        ];

        return response($trainers , 0);
    }

    #[RequestMapping(path: "/api/personal-trainer/{id}/schedule/{sched}", methods: "get")]
    public function scdetail($id, $sched, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $personal = Trainer::with('level')->with('category')->find($id);
        if(!$personal) return response("personal trainer not found", 404);

        $is_booked = false;
        $total_join = $booked_sess = 0;

        $booked = TrainerBooked::select('schedule_date','sessions', 'created_at')
        ->where(['user_id'=>$user->id, 'trainer'=>$id])
        ->first();

        if(isset($booked->sessions)){
            $is_booked = true;
            $booked_sess = (int)$booked->sessions;
            $jodata = TrainerJoined::where(['user_id'=>$user->id, 'trainer_id'=>$id])
            ->count();
            if($jodata && $jodata > $booked->sessions) $jodata = $booked->sessions;
            $total_join = ($jodata) ? (int)$jodata : 0;
        }

        $schedule = TrainerSchedule::with('trainer')->find($sched);
        if(!$schedule) return response("schedule not found", 404);
        // $room  = $schedule->trainer;

        $whr =[
            'schedule_id'=>$sched,
            'trainer_id'=>$id,
            'user_id'=>$user->id
        ];

        $joined = TrainerJoined::select('id', 'trainer_id', 'schedule_id', 'schedule_date', 'attended_at', 'created_at')
        ->where($whr)
        ->first();
        // if(!$joined){
        //     return response("this user doesn't have any appointment with this development schedule", 404);
        // }

        $schedule->is_joined = (!$joined) ? false : true;
        if(isset($schedule->trainer->name)){
            $schedule->trainer->is_booked = $is_booked;
            $schedule->trainer->booked_session = $booked_sess;
            $schedule->trainer->total_join = $total_join;
        }

        return response(new TrainerScheduleResource($schedule), 0);
    }

    #[RequestMapping(path: "/api/personal-training/{id}/book", methods: "post")]
    public function book($id, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        // $setting = Setting::where(['key'=>'max_ncpt'])->first();
        // if(!($setting->value)) return response('need to setup max_ncpt first', 400);
        // $max_ncpt = $setting->value;

        //checker penalty
        $ismb=false;
        if($user->type != 2){
            $plan = PlanSubscription::where(['user_id'=>$user->id])
                    ->where('ends_at', '>', date("Y-m-d H:i:s"))
                    ->first();
            if(isset($plan->id)) $ismb=true;
        }else $ismb = true;

        if(!$ismb){
            return response('this user isnt buy any membership yet', 401);
        }

        $this->validator->make($request->all(), [
            'sessions' => 'required|numeric',
            'channel' => 'required'
        ])->validate();

        // if(! password_verify($request->input('password'), $user->password)) {
        //     return response('Invalid password.', ErrorCode::FORM_ERROR);
        // }

        $trainer = Trainer::with('level')->with('category')->find($id);
        if(!$trainer){
            return response('trainer not found', 404);
        }

        $jmlsesi = $request->input('sessions');

        $schedule = TrainerPackage::where(['sessions'=>$jmlsesi]);
        if(!$schedule){
            return response("invalid sessions count", ErrorCode::FORM_ERROR);
        }

        //checker type sesuai
        // $session_price = $request->input('sessions') * $trainer->level->price;
        $whl = ['level_id'=>$trainer->level, 'sessions'=>$jmlsesi];
        $lesson = TrainerLevelSession::where($whl)->first();
        if(!isset($lesson->id)){
            return response($trainer->level, 400);
        }

        $session_price = $lesson->price;

        // if($user->balance < $session_price) {
        //     return response("insufficient balance");
        // }

        // if(!isset($trainer->level->price)){
        //     $level = TrainerLevel::find($trainer->level);
        //     $session_price = $request->input('sessions') * $level->price;
        // }

        if(empty($session_price)) return response($trainer->level, 400);

        //checker boo
        $allbook = TrainerBooked::where(['user_id'=>$user->id, 'trainer'=>$id, ])
        ->whereDate('expired_at', '>=', date('Y-m-d'))
        ->first();
        if(isset($allbook->id)) {
            // user beli class
            return response('you already hire this trainer', 400);
        }

        // $user->withdraw($session_price, [
        //     'trainer' => $id,
        //     'sessions' => $request->input('sessions'),
        //     'description' => 'book personal trainer success'
        // ]);

        $tradata = (is_object($trainer)) ? $trainer->toArray() : [];
        $amount = $session_price;

        if(is_array($tradata)){
          $tradata['sessions'] = $request->input('sessions');
          $tradata["level_sessions"] = $lesson->toArray();
        }
        $fee = (!empty($request->input('fee'))) ? $request->input('fee') : 0;
        $trans = PaymentTransaction::create([
            'user_id' => $user->id,
            'payable_id' => $id,
            'payable_type' => (is_object($trainer)) ? get_class($trainer) : "App\Model\TrainerBooked",
            'status'  => 0,
            'channel' => $request->input('channel'),
            'amount' => $session_price,
            'fee' => $fee,
            'expire_in' => Carbon::now()->addMinutes(config('pga.duitku.expire_in')),
            'product' => $tradata
        ]);

        $format = str_pad((string) $trans->id, 6, '0', STR_PAD_LEFT);
        $invoice = "TB/{$trans->created_at->format('Ymd')}/{$format}";
        $trans->invoice_no = $invoice;
        $trans->save();

        $payment = (new Duitku)->create([
            'invoice_no' => $trans->invoice_no,
            'channel' => $trans->channel,
            'amount' => $trans->amount,
            'description' => 'Book Trainer - '.$trainer->name.' '.$request->input('sessions').' session',
            'user' => [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phoneFormat,
                'address' => $user->address
            ],
            'items' => [
                [
                    'name' => $trainer->name,
                    'price' => $amount,
                    'quantity' => 1
                ]
            ]
        ]);

        // return response(new TrainerBookedResource($booked));

        $trans->update(['payment_create' => $payment, 'reference_no' => $payment['reference']]);

        // $booked = TrainerBooked::create([
        //     'user_id' => $user->id,
        //     'trainer' => $id,
        //     'sessions' => $request->input('sessions')
        // ]);

        return response(new PaymentTransactionResource($trans));
    }

    #[RequestMapping(path: "/public/{usr}/personal-trainer/{id}/book/{session}/{channel}/{sales}/{fee}", methods: "get")]
    public function pbook($usr, $id, $session, $channel, $sales, $fee, RequestInterface $request)
    {
        $user = User::find($usr);
        if(!$user){
            return response('user not found', 404);
        }

        //checker penalty
        $ismb=false;
        if($user->type != 2){
            $plan = PlanSubscription::where(['user_id'=>$user->id])
                    ->where('ends_at', '>', date("Y-m-d H:i:s"))
                    ->first();
            if(isset($plan->id)) $ismb=true;
        }else $ismb = true;

        if(!$ismb){
            return response('this user isnt buy any membership yet', 401);
        }


        $trainer = Trainer::with('level')->with('category')->find($id);
        if(!$trainer){
            return response('trainer not found', 404);
        }

        $jmlsesi = $session;

        $schedule = TrainerPackage::where(['sessions'=>$jmlsesi]);
        if(!$schedule){
            return response("invalid sessions count", ErrorCode::FORM_ERROR);
        }

        //checker type sesuai
        // $session_price = $request->input('sessions') * $trainer->level->price;
        $whl = ['level_id'=>$trainer->level, 'sessions'=>$jmlsesi];
        $lesson = TrainerLevelSession::where($whl)->first();
        if(!isset($lesson->id)){
            return response($trainer->level, 400);
        }

        $session_price = $lesson->price;

        // if($user->balance < $session_price) {
        //     return response("insufficient balance");
        // }

        // if(!isset($trainer->level->price)){
        //     $level = TrainerLevel::find($trainer->level);
        //     $session_price = $request->input('sessions') * $level->price;
        // }

        if(empty($session_price)) return response($trainer->level, 400);

        //checker boo
        $allbook = TrainerBooked::where(['user_id'=>$user->id, 'trainer'=>$id, ])
        ->whereDate('expired_at', '>=', date('Y-m-d'))
        ->first();
        if(isset($allbook->id)) {
            // user beli class
            return response('you already hire this trainer', 400);
        }

        // $user->withdraw($session_price, [
        //     'trainer' => $id,
        //     'sessions' => $request->input('sessions'),
        //     'description' => 'book personal trainer success'
        // ]);

        $tradata = (is_object($trainer)) ? $trainer->toArray() : [];
        $amount = $session_price;

        if(is_array($tradata)){
          $tradata['sessions'] = $jmlsesi;
          $tradata['sessional'] = $jmlsesi;
          $tradata["level_sessions"] = $lesson->toArray();
          if(!empty($sales)){
              $tradata["sales"] = $sales;
          }
        }
        $trans = PaymentTransaction::create([
            'user_id' => $user->id,
            'payable_id' => $id,
            'payable_type' => (is_object($trainer)) ? get_class($trainer) : "App\Model\TrainerBooked",
            'status'  => 0,
            'channel' => $channel,
            'amount' => $session_price,
            'fee' => $fee,
            'expire_in' => Carbon::now()->addMinutes(config('pga.duitku.expire_in')),
            'product' => $tradata
        ]);

        $format = str_pad((string) $trans->id, 6, '0', STR_PAD_LEFT);
        $invoice = "TB/{$trans->created_at->format('Ymd')}/{$format}";
        $trans->invoice_no = $invoice;
        $trans->save();

        if(!empty($channel) && $channel != "CASH"){
            $payment = (new Duitku)->create([
                'invoice_no' => $trans->invoice_no,
                'channel' => $trans->channel,
                'amount' => $trans->amount,
                'description' => 'Book Trainer - '.$trainer->name.' '.$session.' session',
                'user' => [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'email' => $user->email,
                    'phone' => $user->phoneFormat,
                    'address' => $user->address
                ],
                'items' => [
                    [
                        'name' => $trainer->name,
                        'price' => $amount,
                        'quantity' => 1
                    ]
                ]
            ]);

            // return response(new TrainerBookedResource($booked));
            $trans->update(['payment_create' => $payment, 'reference_no' => $payment['reference']]);
        }else{
            $trans->update(['status'=>1]);
            $this->event->dispatch(new PaymentSuccess($trans));
        }

        // $booked = TrainerBooked::create([
        //     'user_id' => $user->id,
        //     'trainer' => $id,
        //     'sessions' => $request->input('sessions')
        // ]);

        return response(new PaymentTransactionResource($trans));
    }

    #[RequestMapping(path: "/api/trainer/schedule/join", methods: "post")]
    public function tbook(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'trainer_id' => 'required|exists:trainers,id',
            'schedule_id' => 'required|exists:trainer_schedules,id',
            'password' => 'required|min:6'
        ])->validate();

        if(! password_verify($request->input('password'), $user->password)) {
            return response('Invalid password.', ErrorCode::FORM_ERROR);
        }

        $trainer = Trainer::find($request->input('trainer_id'));
        if(!isset($trainer->id)){
            return response('trainer not found', 404);
        }

        $schedule = TrainerSchedule::find($request->input('schedule_id'));
        $room  = $schedule->trainer;

        $wht = [
          'trainer'=>$trainer->id,
          'user_id'=>$user->id
        ];
        $booked = TrainerBooked::where($wht)->first();
        if(!isset($booked->id)){
            return response('this user has not book this trainer yet', 401);
        }else{
          if(empty($booked->expired_at) || $booked->expired_at <= date("Y-m-d H:i:s")){
              return response('your book for this trainer has been expired', 401);
          }
        }

        $whr =[
          'schedule_id'=>$schedule->id,
          'trainer_id'=>$request->input('trainer_id'),
          'user_id'=>$user->id
        ];

        $joined = TrainerJoined::where($whr)->first();
        if(isset($joined->id)){
            return response("this user already book with this schedule", ErrorCode::FORM_ERROR);
        }else if($schedule->schedule_date->format("Y-m-d") < date("Y-m-d")){
            return response("you can't join past class", 401);
        }

        $joinned = $user->bookPT($schedule);

        $cjoined = TrainerJoined::where($whr)
        ->with('trainer')
        ->with('schedule')
        ->first();
        if(isset($cjoined->id)){
            Mail::to($user->email)->send(new BookTrainer($user, $cjoined));
            $usname = $user->first_name.' '.$user->last_name;
            $title = $usname.', Success Booking of '.$cjoined->trainer->name;
            $body = 'you have book this trainer '.$cjoined->trainer->name.' at '.$schedule->schedule_date;

            $notif = [
                'title' =>$title,
                'body' =>$body,
                'alert' => $title,
                'icon' => 'https://placekitten.com/100/100',
                'sound' => "default",
            ];
            $data_notif = [
                'title'=>$title,
                'body'=>$body,
                'icon' => 'https://placekitten.com/100/100',
                'priority' => 'high',
                'content_available' => true
            ];
            $notifs = (new Fcm)->toTopic('user_'.$user->id)
            ->notification($notif)
            ->data($data_notif)
            ->send();
            $cjoined->data_notif = $notifs;
            $data = [
              'title' => $title,
              'message' => $body,
              'data' => $cjoined
            ];
            Notification::create([
              'id' =>  Db::select("SELECT UUID() AS uuid_string")[0]->uuid_string,
              'user_id' => $user->id,
              'activity' => "trainer-booking",
              'data' => json_encode($data),
              'requests' => ['notif'=>$notif, 'data'=>$data_notif],
              'responses'=> $notifs
            ]);
        }

        return response($cjoined, 0);
    }

    #[RequestMapping(path: "/public/trainer/schedule/join", methods: "post")]
    public function ptbook(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'user' => 'required|exists:users,id',
            'trainer' => 'required|exists:trainers,id',
            'schedule' => 'required|exists:trainer_schedules,id'
        ])->validate();

        $user = User::find($request->input('user'));
        if(!$user){
            response('user not found', 404);
        }

        $trainer = Trainer::find($request->input('trainer'));
        if(!isset($trainer->id)){
            return response('trainer not found', 404);
        }

        $schedule = TrainerSchedule::find($request->input('schedule'));
        if(!isset($schedule->id)){
            return response('no schedule found for this trainer', 404);
        }

        $room  = $schedule->trainer;

        $wht = [
          'trainer'=>$trainer->id,
          'user_id'=>$user->id
        ];
        $booked = TrainerBooked::where($wht)->first();
        if(!isset($booked->id)){
            return response('this user has not book this trainer yet', 401);
        }else{
          if(empty($booked->expired_at) || $booked->expired_at <= date("Y-m-d H:i:s")){
              return response('your book for this trainer has been expired', 401);
          }
        }

        $whr =[
          'schedule_id'=>$schedule->id,
          'trainer_id'=>$trainer->id,
          'user_id'=>$user->id
        ];

        $joined = TrainerJoined::where($whr)->first();
        if(isset($joined->id)){
            return response("this user already book with this schedule", ErrorCode::FORM_ERROR);
        }

        $joinned = $user->bookPT($schedule);

        $cjoined = TrainerJoined::where($whr)
        ->with('trainer')
        ->with('schedule')
        ->first();
        if(isset($cjoined->id)){
            Mail::to($user->email)->send(new BookTrainer($user, $cjoined));
            $usname = $user->first_name.' '.$user->last_name;
            $title = $usname.', Success Booking of '.$cjoined->trainer->name;
            $body = 'you have book this trainer '.$cjoined->trainer->name.' at '.$schedule->schedule_date;

            $notif = [
                'title' =>$title,
                'body' =>$body,
                'alert' => $title,
                'icon' => 'https://placekitten.com/100/100',
                'sound' => "default",
            ];
            $data_notif = [
                'title'=>$title,
                'body'=>$body,
                'icon' => 'https://placekitten.com/100/100',
                'priority' => 'high',
                'content_available' => true
            ];
            $notifs = (new Fcm)->toTopic('user_'.$user->id)
            ->notification($notif)
            ->data($data_notif)
            ->send();
            $cjoined->data_notif = $notifs;
            $data = [
              'title' => $title,
              'message' => $body,
              'data' => $cjoined
            ];
            Notification::create([
              'id' =>  Db::select("SELECT UUID() AS uuid_string")[0]->uuid_string,
              'user_id' => $user->id,
              'activity' => "trainer-booking",
              'data' => json_encode($data),
              'requests' => ['notif'=>$notif, 'data'=>$data_notif],
              'responses'=> $notifs
            ]);
        }

        return response($cjoined, 0);
    }

    #[RequestMapping(path: "/api/trainer/{id}/schedule/{sched}/attend", methods: "put")]
    public function attend($id, $sched, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $schedule = TrainerJoined::where([
            'user_id' => $user->id,
            'schedule_id' => $sched
        ])
        ->with('trainer')
        ->with('schedule')
        ->first();

        if(!$schedule->schedule_id){
          return response("Schedule not found", 404);
        }else if(!empty($schedule->attended_at)){
          return response("You can't attend something that you had attended", 400);
        }

        $schedule->forceFill([
            'attended_at' => Carbon::now()->format('Y-m-d H:i:s')
        ])->save();

        return response($schedule);
    }

    #[RequestMapping(path: "/api/activities/training", methods: "post")]
    public function activity(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();
        $date = $request->input('date', Carbon::now()->format('Y-m-d'));

        $month = Carbon::parse($date);

        $activities = $user->tschedules()
            ->with('trainer')
            ->whereYear('trainer_schedules.schedule_date', '=', $month->format('Y'))
            ->whereMonth('trainer_schedules.schedule_date', '=', $month->format('m'))
            ->get();

        return response($activities);
    }

    #[RequestMapping(path: "/api/trainer/{id}/all-schedule", methods: "post")]
    public function allsched($id, RequestInterface $request)
    {

        $user = $this->auth->guard()->user();

        $is_booked = false;
        $total_join = $booked_sess = 0;

        $booked = TrainerBooked::select('schedule_date','sessions', 'created_at')
        ->where(['user_id'=>$user->id, 'trainer'=>$id])
        ->first();

        if(isset($booked->sessions)){
            $is_booked = true;
            $booked_sess = (int)$booked->sessions;
            $jodata = TrainerJoined::where(['user_id'=>$user->id, 'trainer_id'=>$id])
            ->count();
            if($jodata && $jodata > $booked->sessions) $jodata = $booked->sessions;
            $total_join = ($jodata) ? (int)$jodata : 0;
        }

        $this->validator->make($request->all(), [
            'date' => 'required'
        ])->validate();

        $date = $request->input('date', Carbon::now()->format('Y-m-d'));
        $month = Carbon::parse($date);

        $trainer = Trainer::with('level')->with('category')->find($id);
        if(!$trainer) return response("trainer not found", 404);

        $trainer->is_book = $is_booked;
        $trainer->booked_session = $booked_sess;
        $trainer->total_join = $total_join;

        $packages = TrainerSchedule::join('trainer_joined', 'trainer_schedules.id', '=', 'trainer_joined.schedule_id')
        ->where('trainer_joined.trainer_id', $id)
        ->whereYear('trainer_schedules.schedule_date', '=', $month->format('Y'))
        ->whereMonth('trainer_schedules.schedule_date', '=', $month->format('m'))
        ->get(['trainer_schedules.*', 'trainer_joined.schedule_date']);

        if(!$packages) return response('personal development schedule is empty', 0);

        $trainers = [
          'trainer' => $trainer,
          'schedule' => TrainerScheduleResource::collection($packages)
        ];

        return response($trainers , 0);
    }

    #[RequestMapping(path: "/api/trainer/{id}/schedule/{sched}/cancel", methods: "put")]
    public function tcancel($id, $sched, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'password' => 'required|min:6'
        ])->validate();

        if(! password_verify($request->input('password'), $user->password)) {
            return response('Invalid password.', ErrorCode::FORM_ERROR);
        }

        $schedule = TrainerSchedule::find($sched);
        if(!$schedule) return response("schedule not found", 404);
        // $room  = $schedule->trainer;

        $whr =[
          'schedule_id'=>$sched,
          'trainer_id'=>$id,
          'user_id'=>$user->id
        ];

        $joined = TrainerJoined::where($whr)->first();
        $cjoined = $joined;
        if(!$joined){
            return response("this user doesn't have any appointment with this trainer schedule", ErrorCode::FORM_ERROR);
        }else if(!empty($joined->attended_at)){
            return response("you can't cancel something that you already attend", ErrorCode::FORM_ERROR);
        }else if($joined->schedule_date->format("Y-m-d") < date("Y-m-d")){
            return response("you can't cancel something that already in the past", ErrorCode::FORM_ERROR);
        }

        $joined->delete();

        $trainer = Trainer::find($id);

        Mail::to($user->email)->send(new CancelBookTrainer($user, $schedule));
        $title = 'Hi, '.$user->first_name.', you are cancel booking of '.$trainer->name;
        $body = 'you cancel book this trainer '.$trainer->name.' at '.$schedule->schedule_date;
        $notif = [
            'title' =>$title,
            'body' =>$body,
            'alert' => $title,
            'icon' => 'https://placekitten.com/100/100',
            'sound' => "default",
        ];
        $data_notif = [
            'title'=>$title,
            'body'=>$body,
            'icon' => 'https://placekitten.com/100/100',
            'priority' => 'high',
            'content_available' => true
        ];
        $notifs = (new Fcm)->toTopic('user_'.$user->id)
        ->notification($notif)
        ->data($data_notif)
        ->send();
        $data = [
            'title' => $title,
            'message' => $body,
            'data' => $cjoined
        ];
        Notification::create([
            'id' =>  Db::select("SELECT UUID() AS uuid_string")[0]->uuid_string,
            'user_id' => $user->id,
            'activity' => "trainer-cancel",
            'data' => json_encode($data),
            'requests' => ['notif'=>$notif, 'data'=>$data_notif],
            'responses'=> $notifs
        ]);

        return response("successfully cancel appointment", 0);
    }
}

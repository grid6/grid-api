<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\Media;
use Hyperf\Utils\Str;
use Hyperf\Di\Annotation\Inject;
use League\Flysystem\Filesystem;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpMessage\Stream\StandardStream;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;

#[Controller]
class MediaController
{
    #[Inject]
    protected Filesystem $filesystem;

    #[Inject]
    protected ResponseInterface $response;

    #[Inject]
    protected RequestInterface $request;

    #[RequestMapping(path: "/upload", methods: "post")]
    public function upload()
    {
        $user = $this->request->getAttribute('user');
        $userPath = $user ? $user->id : 'public';

        $file = $this->request->file('file');
        
        if(is_null($file)) {
            return response(__("Invalid file format"), 422); 
        }

        if( $file && ! $file->isValid()) {
            return response(__("Invalid file format"), 422);
        }

        $filename = Str::random(40) . '.'. $file->getExtension();
        $folder = "upload/{$userPath}"; 
        $filepath = $folder . '/' . $filename;
        
        $stream = fopen($file->getRealPath(), 'r+');
        $this->filesystem->writeStream(
            $filepath,
            $stream
        );
        fclose($stream);
        
        $path = "/file/" . $filepath;
        $driver = config('file.default');

        $media = Media::create([
            'path' => $path,
            'host' => config('file.host') ?? null,
            'driver' => $driver,
            'user_id' => $user ? $user->id: null,
        ]);

        return response($media);
    }

    #[RequestMapping(path: "/file/{path:.+}", methods: "get")]
    public function file($path)
    {
        $stream = $this->filesystem->read($path);
        $mimetype = $this->filesystem->mimeType($path);
        $filesize = $this->filesystem->fileSize($path);
        
        return $this->response->withHeader('Server', 'Hyperf')
                ->withHeader('Content-Type', $mimetype)
                ->withHeader('Content-Length', $filesize)
                ->withBody(StandardStream::create($stream));
    }
}

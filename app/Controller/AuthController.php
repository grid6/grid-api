<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use App\Pos\Grids;
use App\Notification\Fcm;
use App\Model\User;
use App\Event\Verified;
use HyperfExt\Mail\Mail;
use HyperfExt\Mail\PendingMail;
use App\Event\Registered;
use App\Mail\VerifyEmail;
use App\Constants\ErrorCode;
use App\Mail\ForgotPassword;
use App\Resource\UserResource;
use Hyperf\Di\Annotation\Inject;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use Qbhy\HyperfAuth\AuthManager;
use \Mailjet\Resources;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;

#[Controller]
class AuthController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected EventDispatcherInterface $event;

    protected int $ttl;

    public function __construct()
    {
        $default = config('auth.default.guard');
        $ttl = config("auth.guards.{$default}.refresh_ttl");

        $this->ttl = $ttl;
    }

    #[RequestMapping(path: "/api/auth", methods: "post")]
    public function signIn(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ])->validate();

        $deviceId = $request->header('GRID-DID');

        $user = User::whereEmail($request->input('email'))->first();
        if(!$user->id){
            return response('User not registered', ErrorCode::DATA_NOTFOUND);
        }

        if($user->status == 3){
            return response('User is blocked or already deactivate, contact receptionist', ErrorCode::DATA_NOTFOUND);
        }

        if(! password_verify($request->input('password'), $user->password)) {
            return response('Invalid password.', ErrorCode::FORM_ERROR);
        }

        $token = $this->auth->login($user);

        $notif = (new Fcm)->subTopic($deviceId, 'user_'.$user->id);

        return [
            'token_type' => 'Bearer',
            'expires_in' => $this->ttl,
            'access_token' => $token,
            'user' => new UserResource($user)
        ];
    }

    #[RequestMapping(path: "/api/me", methods: "get")]
    public function me(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $ismembership = false;
        $dataplan = null;
        $plan = PlanSubscription::latest()
        ->with('plan')
        ->where(['user_id'=>$user->id])
        ->where('ends_at', '>', date("Y-m-d H:i:s"))
        ->first();
        if(isset($plan->id)){
            $ismembership=true;
            $dataplan = [
              'tag'=>$plan->plan->tag,
              'ends_at'=> Carbon::parse($plan->ends_at)->format('d/m/Y')
            ];
            // return response('this user isnt membership');
        }

        if($user->type == 2){
          $ismembership = true;
        }

        $userdata = [
            'id'=>$user->id,
            'first_name' => (string) $user->first_name,
            'last_name' => (string) $user->last_name,
            'email' => (string) $user->email,
            'phone' => (string) $user->phone,
            'gender' => (string) $user->gender,
            'photo' => (string) $user->photo,
            'address' => (string) $user->address,
            'type' => (int) $user->type,
            'verified' => (bool) ! is_null($user->email_verified_at),
            'is_membership' => $ismembership,
            'data_plan'=>$dataplan
        ];

        return response($userdata, 0);
    }

    #[RequestMapping(path: "/api/me", methods: "put")]
    public function update(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();
        $user->fill($request->all());

        if($request->has('password')) {
            $user->password = password_hash($request->input('password'), PASSWORD_BCRYPT, ['cost' => 10]);
        }

        $user->save();

        $ismembership = false;
        if($user->type != 2){
            $plan = PlanSubscription::where(['user_id'=>$user->id])->first();
            if(isset($plan->id)){
                $ismembership=true;
                // return response('this user isnt membership');
            }
        }else $ismembership = true;

        $userdata = [
            'id'=>$user->id,
            'first_name' => (string) $user->first_name,
            'last_name' => (string) $user->last_name,
            'email' => (string) $user->email,
            'phone' => (string) $user->phone,
            'gender' => (string) $user->gender,
            'photo' => (string) $user->photo,
            'address' => (string) $user->address,
            'type' => (int) $user->type,
            'verified' => (bool) ! is_null($user->email_verified_at),
            'is_membership' => $is_membership
        ];

        return response($userdata, 0);
    }

    #[RequestMapping(path: "/api/close-account", methods: "put")]
    public function close(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'password'   => 'required|min:6'
        ])->validate();

        $user = $this->auth->guard()->user();

        if(!password_verify($request->input('password'), $user->password)) {
            return response('Invalid password.', ErrorCode::FORM_ERROR);
        }

        $user->status = 3;
        $user->save();

        $this->auth->guard()->logout();

        return response(new UserResource($user));
    }

    #[RequestMapping(path: "/auth/refresh", methods: "post")]
    public function refresh(RequestInterface $request)
    {
        try {
            $token = $this->auth->guard()->refresh();
            $user = $this->auth->guard()->user($token);

            return [
                'token_type' => 'Bearer',
                'expires_in' => $this->ttl,
                'access_token' => $token,
                'user' => new UserResource($user),
            ];
        } catch (\Throwable $th) {
            return response($th->getMessage(), ErrorCode::FORM_ERROR);
        }
    }

    #[RequestMapping(path: "/api/register", methods: "post")]
    public function register(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'first_name' => 'required',
            'phone_code' => 'required',
            'phone'      => 'required|min:10',
            'email'      => 'required|email|unique:users',
            'password'   => 'required|min:6'
        ])->validate();




        $user = User::create(array_merge($request->all(), [
            'type' => 1,
            'status' => 0,
            'email_verified_at' => date("Y-m-d H:i:s"),
            'password' => password_hash($request->input('password'), PASSWORD_BCRYPT, ['cost' => 10])
        ]));

        $this->event->dispatch(new Registered($user), 1);

        $allreq = $request->all();
        $allreq["id"] = $user->id;
        $allreq["gender"] = (isset($allreq["gender"]) && strtolower($allreq["gender"]) == "female") ? "P" : "L";
        $payment = (new Grids)->createCust($allreq);

        // return response($payment, 0);


        return response(new UserResource($user));
    }

    #[RequestMapping(path: "/api/verify/send-email", methods: "post")]
    public function sendVerifyEmail(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'email' => 'required|exists:users'
        ])->validate();

        $user = User::whereEmail($request->input('email'))->first();

        if(! is_null($user->email_verified_at)) {
            return response('Your account has been verified');
        }

        Mail::to($user->email)->send(new VerifyEmail($user));

        return response('Please check your email <b>'. $user->email .'</b>');
    }

    #[RequestMapping(path: "/api/verify", methods: "post")]
    public function verifyEmail(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'email' => 'required|exists:users',
            'code'  => 'required'
        ])->validate();

        $user = User::whereEmail($request->input('email'))->first();

        if($request->input('code') != sha1($user->email)) {
            return response('invalid verification code', ErrorCode::FORM_ERROR);
        }

        if($user->emailVerified()) {
            $this->event->dispatch(new Verified($user), 1);
        }

        return response(new UserResource($user));
    }

    #[RequestMapping(path: "/api/forgot-password", methods: "post")]
    public function forgotPassword(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'email' => 'required|exists:users'
        ])->validate();

        $user = User::whereEmail($request->input('email'))->first();
        if(!$user->id){
            return response('User not registered', ErrorCode::DATA_NOTFOUND);
        }

        Mail::to($user->email)->send(new ForgotPassword($user));

        $code = base64_encode($request->input('email'));
        return response('code for : '. $user->email .' is '.$code);
    }

    #[RequestMapping(path: "/api/forgot-change-password", methods: "post")]
    public function changeForgotPassword($code, RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'code' => 'required',
            'password' => 'required|min:6|confirmed',
        ])->validate();

        $user = User::whereEmail($request->input('email'))->first();
        if(!$user->id){
            return response('User not registered', ErrorCode::DATA_NOTFOUND);
        }
        $user->update(['password'=>password_hash($request->input('password'), PASSWORD_BCRYPT, ['cost' => 10])]);

        return response('password has been reset');
    }

    #[RequestMapping(path: "/api/forgot-change-password/{code}", methods: "post")]
    public function changeForgotPasswordwithCode($code, RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'password' => 'required|min:6',
        ])->validate();

        $user = User::whereEmail(base64_decode($code))->first();
        if(!$user->id){
            return response('User not registered', ErrorCode::DATA_NOTFOUND);
        }
        $user->update(['password'=>password_hash($request->input('password'), PASSWORD_BCRYPT, ['cost' => 10])]);

        return response('password has been reset');
    }

    #[RequestMapping(path: "/api/change-password", methods: "post")]
    public function changePassword(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'old_password' => 'required|min:6',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6',
        ])->validate();

        $user = $this->auth->guard()->user();

        if(!password_verify($request->input('old_password'), $user->password)) {
            return response('Invalid current password.', ErrorCode::FORM_ERROR);
        }

        if($request->input('password') != $request->input('confirm_password')){
            return response('confirm password must be same with password.', ErrorCode::FORM_ERROR);
        }

        $user->password = password_hash($request->input('password'), PASSWORD_BCRYPT, ['cost' => 10]);
        $user->save();

        return response('password has been updated');
    }

    #[RequestMapping(path: "/api/change-phone", methods: "post")]
    public function changePhone(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'old_phone'      => 'required|min:10',
            'phone'      => 'required|min:10'
        ])->validate();

        $user = $this->auth->guard()->user();

        if($request->input('old_phone') != $user->phone){
            return response('miss-match old phone', ErrorCode::FORM_ERROR);
        }

        $user->phone = $request->input('phone');
        $user->save();

        return response('your phone number has been updated');
    }

    #[RequestMapping(path: "/api/change-email", methods: "post")]
    public function changeMail(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'email'      => 'required|min:10|unique:users'
        ])->validate();

        $user = $this->auth->guard()->user();

        $user->email = $request->input('email');

        $user->save();

        return response('your e-mail has been updated');
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use App\Payment\Duitku;
use App\Model\ClassRoom;
use App\Model\Category;
use App\Model\ClassRoomJoined;
use App\Model\ClassRoomSchedule;
use App\Model\WaitList;
use App\Constants\ErrorCode;
use App\Resource\ClassResource;
use App\Resource\CategoryResource;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use App\Model\PaymentTransaction;
use App\Resource\ActivityResource;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;


#[Controller]
class WaitlistController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected EventDispatcherInterface $event;

    #[RequestMapping(path: "/api/class/{class}/schedule/{id}/waitlist", methods: "get")]
    public function wait($class, $id, RequestInterface $request)
    {

        $user = $this->auth->guard()->user();

        $row = ClassRoom::with('schedules')->find($class);
        if(!$row->id){
            return response('data class not found', ErrorCode::DATA_NOTFOUND);
        }

        $row2 = ClassRoomSchedule::with('class')->find($id);
        if(!$row2->id){
            return response('data schedule not found', ErrorCode::DATA_NOTFOUND);
        }

        $row3 = ClassRoomJoined::with('class')
                ->where(['is_waiting_list'=>1, 'id'=>$id])
                ->get();

        return response($row3, 0);
    }
}

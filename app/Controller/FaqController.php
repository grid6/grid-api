<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use App\Xmqtt\Mqtt;
use App\Model\Faq;
use App\Constants\ErrorCode;
use App\Resource\FaqResource;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;


#[Controller]
class FaqController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected EventDispatcherInterface $event;

    #[RequestMapping(path: "/api/faq", methods: "get")]
    public function index(RequestInterface $request)
    {
        $search = $request->input("search");

        if(!empty($search)){
            $search = urldecode($search);
            $faq = Faq::where('question', 'LIKE', '%'.$search.'%')
            ->orWhereRaw("FIND_IN_SET('$search', keywords) > 0")
            ->get();
        }else{
            $faq = Faq::all();
        }

        if(!$faq){
            return response('faq is empty', 0);
        }

        return response(FaqResource::collection($faq));
    }

    #[RequestMapping(path: "/api/enroll", methods: "get")]
    public function enroll(RequestInterface $request)
    {
        $faq = [];
        $device = (new Mqtt)->devices();
        $user = $this->auth->guard()->user();
        if(!empty($device) && isset($device['data'])){
            $devs = $respz = [];
            if(!empty($user->face_registered)){
              foreach($device['data'] as $dvc){
                  array_push($devs, $dvc['id']);
                  // Decode the base64 image string
                    $param = [
                      'id'=>$user->id,
                      'device'=>$dvc['id'],
                      'first_name' => $user->first_name,
                      'last_name' => (string)$user->last_name,
                      'face_registered'=>$user->face_registered
                    ];
                    $respz = (new Mqtt)->enroll($param);
              }
            }
            $faq['device'] = $devs;
            $faq['response'] = $respz;
            $faq['user'] = [
              'id' => $user->id,
              'name' => $user->first_name." ".$user->last_name,
              'face_registered' => $user->face_registered
            ];
        }

        if(count($faq) == 0){
            return response('device is empty', 0);
        }

        return response($faq);
    }

}

<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use App\Model\Page;
use App\Model\Setting;
use App\Constants\ErrorCode;
use App\Resource\PageResource;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;


#[Controller]
class PageController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected EventDispatcherInterface $event;

    #[RequestMapping(path: "/api/page/{type}/{slug}", methods: "get")]
    public function index($type, $slug, RequestInterface $request)
    {
        $page = Page::where('type', $type)
        ->where('slug', $slug)
        ->get();

        if(!$page){
            return response('page is empty', 0);
        }

        return response(PageResource::collection($page));
    }

    #[RequestMapping(path: "/api/settings", methods: "get")]
    public function settings(){
        $setup = [];
        $setting = Setting::where(['group'=>'apps'])->get();
        if(!$setting) return response('setting still empty', 0);
        foreach($setting as $sett){
            $setup[$sett->key] = $sett->value;
        }
        return response($setup, 0);
    }
}

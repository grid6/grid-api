<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use App\Model\Personal;
use App\Model\PersonalBooked;
use App\Model\PersonalPackage;
use App\Payment\Duitku;
use App\Constants\ErrorCode;
use App\Resource\PersonalResource;
use App\Resource\PersonalBookedResource;
use App\Resource\PersonalPackageResource;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use App\Model\PaymentTransaction;
use App\Resource\PaymentTransactionResource;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;


#[Controller]
class PersonalController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected EventDispatcherInterface $event;

    #[RequestMapping(path: "/api/development", methods: "get")]
    public function indeks(RequestInterface $request)
    {
        if (isset($_SERVER['Authorization']) && !empty($_SERVER['Authorization'])) {
            $user = $this->auth->guard()->user();
        }

        $search = $request->input("search");

        if(!empty($search)){
            $search = urldecode($search);
            $personal = Personal::where('name', 'LIKE', '%'.$search.'%')
            ->get();
        }else{
            $personal = Personal::all();
        }

        if(!$personal){
            return response('personal development is empty', 0);
        }

        return response($personal);
    }

    #[RequestMapping(path: "/api/personal-development", methods: "get")]
    public function index(RequestInterface $request)
    {
        if (isset($_SERVER['Authorization']) && !empty($_SERVER['Authorization'])) {
            $user = $this->auth->guard()->user();
        }

        $search = $request->input("search");

        if(!empty($search)){
            $search = urldecode($search);
            $personal = Personal::where('name', 'LIKE', '%'.$search.'%')
            ->get();
        }else{
            $personal = Personal::all();
        }

        if(!$personal){
            return response('personal development is empty', 0);
        }

        return response(PersonalResource::collection($personal));
    }

    #[RequestMapping(path: "/api/personal-development/{id}", methods: "get")]
    public function detail($id, RequestInterface $request)
    {
        $personal = Personal::find($id);
        if(!$personal){
            return response('personal development not found', 404);
        }

        $is_booked = false;
        $book_data = null;
        if (isset($_SERVER['Authorization']) && !empty($_SERVER['Authorization'])) {
            $user = $this->auth->guard()->user();
            $booked = PersonalBooked::select('schedule_date','sessions', 'created_at')
            ->where(['user_id'=>$user->id, 'personal'=>$id])
            ->first();
            if(isset($booked->sessions)){
                $is_booked = true;
                $book_data = $booked;
            }
        }

        $personal->is_book = $is_booked;
        $personal->book_data = $booked;

        return response(new PersonalResource($personal));
    }

    #[RequestMapping(path: "/api/personal-development/{id}/schedule", methods: "post")]
    public function schedule($id, RequestInterface $request)
    {
        $uid = 0;
        if (!empty($request->header("Authorization"))) {
            $user = $this->auth->guard()->user();
            $uid = $user->id;
        }

        $this->validator->make($request->all(), [
            'date' => 'required'
        ])->validate();

        $date = $request->input('date', Carbon::now()->format('Y-m-d'));

        $month = Carbon::parse($date);

        $packages = PersonalPackage::where('personal', $id)
        ->whereDate('schedule_date', '=', $month->format('Y-m-d'))
        ->get();
        if(!$packages){
            return response('personal development schedule is empty', 0);
        }

        return response(PersonalPackageResource::collection($packages));
    }

    #[RequestMapping(path: "/api/personal-development/{id}/schedule/{sched}", methods: "get")]
    public function scdetail($id, $sched, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $personal = Personal::find($id);
        if(!$personal) return response("personal development not found", 404);

        $schedule = PersonalPackage::with('personal')->find($sched);
        if(!$schedule) return response("schedule not found", 404);
        // $room  = $schedule->trainer;

        $whr =[
            'schedule_id'=>$sched,
            'personal'=>$id,
            'user_id'=>$user->id
        ];

        $joined = PersonalBooked::select('id', 'personal', 'schedule_id', 'schedule_date', 'attended_at', 'created_at')
        ->where($whr)
        ->first();
        // if(!$joined){
        //     return response("this user doesn't have any appointment with this development schedule", 404);
        // }

        $schedule->is_booked = (!$joined) ? false : true;

        return response($schedule, 0);
    }

    #[RequestMapping(path: "/api/personal-development/{id}/book", methods: "post")]
    public function book($id, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'schedule' => 'required'
        ])->validate();

        // if(! password_verify($request->input('password'), $user->password)) {
        //     return response('Invalid password.', ErrorCode::FORM_ERROR);
        // }

        $schedule = PersonalPackage::find($request->input('schedule'));
        if(!$schedule){
            return response("schedule not found", ErrorCode::FORM_ERROR);
        }

        //checker type sesuai
        $personal = Personal::find($id);
        if(!$personal->id){
            return response("data personal development not found", 404);
        }

        $perdata = (is_object($personal)) ? $personal->toArray() : [];

        if(is_array($perdata)) $perdata['sessions'] = $request->input('sessions');

        //checker status user

        if($user->type != 2){
            $plan = PlanSubscription::where(['user_id'=>$user->id])->first();
            if(!$plan){
                return response('this user isnt');
            }
        }

        //checker booked
        $allbooked = PersonalBooked::where([
          'user_id'=>$user->id,
          'personal'=>$personal->id,
          'schedule_id'=>$schedule->id,
          'status'=>1
        ])->first();

        if(isset($allbooked->id)) {
            return response('this personal development is already booked', 400);
        }

        $booked = PersonalBooked::create([
            'user_id' => $user->id,
            'personal' => $id,
            'schedule_id'=>$schedule->id,
            'schedule_date'=>$schedule->schedule_date
        ]);

        return response(new PersonalBookedResource($booked));

    }

    #[RequestMapping(path: "/api/personal-development/{id}/schedule/{sched}/attend", methods: "put")]
    public function attend($id, $sched, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $schedule = PersonalBooked::where([
            'user_id' => $user->id,
            'schedule_id' => $sched
        ])
        ->with('personal')
        ->with('schedule')
        ->first();

        if(!$schedule->schedule_id){
          return response("Schedule not found", 404);
        }else if(!empty($schedule->attended_at)){
          return response("You can't attend something that you had attended", 400);
        }

        $schedule->forceFill([
            'attended_at' => Carbon::now()->format('Y-m-d H:i:s')
        ])->save();

        return response($schedule);
    }

    #[RequestMapping(path: "/api/personal-development/{id}/schedule/{sched}/cancel", methods: "put")]
    public function cancel($id, $sched, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'password' => 'required|min:6'
        ])->validate();

        if(! password_verify($request->input('password'), $user->password)) {
            return response('Invalid password.', ErrorCode::FORM_ERROR);
        }

        $schedule = PersonalPackage::find($sched);
        if(!$schedule) return response("schedule not found", 404);
        // $room  = $schedule->trainer;

        $whr =[
            'schedule_id'=>$sched,
            'personal'=>$id,
            'user_id'=>$user->id
        ];

        $joined = PersonalBooked::where($whr)->first();
        if(!$joined){
            return response("this user doesn't have any appointment with this development schedule", ErrorCode::FORM_ERROR);
        }else if(!empty($joined->attended_at)){
            return response("you can't cancel something that you already attend", ErrorCode::FORM_ERROR);
        }else if($joined->schedule_date->format("Y-m-d") < date("Y-m-d")){
            return response("you can't cancel something that already in the past", ErrorCode::FORM_ERROR);
        }

        $joined->delete();

        return response("successfully cancel appointment", 0);
    }
}

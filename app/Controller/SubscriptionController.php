<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use Xtwoend\HySubscribe\Model\Plan;
use Xtwoend\HySubscribe\Model\PlanSubscription;
use App\Model\User;
use App\Model\PaymentTransaction;
use App\Event\PaymentSuccess;
use App\Payment\Duitku;
use App\Constants\ErrorCode;
use App\Resource\PlanResource;
use App\Resource\PaymentTransactionResource;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

#[Controller]
class SubscriptionController
{
    #[Inject]
    protected EventDispatcherInterface $event;

    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[RequestMapping(path: "/api/plan", methods: "get")]
    public function index(RequestInterface $request)
    {
        $rpp = $request->input('per_page', 6);

        $rows = Plan::where('visible', 1)->paginate($rpp);

        return response(PlanResource::collection($rows));
    }

    #[RequestMapping(path: "/api/plan/{tag}", methods: "get")]
    public function pertag($tag, RequestInterface $request)
    {
        $tag = urldecode($tag);
        $rows = Plan::where(['tag'=>$tag, 'visible'=>1])->get();

        return response(PlanResource::collection($rows));
    }

    #[RequestMapping(path: "/api/plan/subscription", methods: "post")]
    public function subscription(RequestInterface $request)
    {
        $this->validator->make($request->all(), [
            'plan_id' => 'required|exists:plans,id',
            'channel' => 'required'
        ])->validate();

        $user = $this->auth->guard()->user();

        // if(! password_verify($request->input('password'), $user->password)) {
        //     return response('Invalid password.', ErrorCode::FORM_ERROR);
        // }

        $plan = Plan::find($request->input('plan_id'));

        if(!$plan){
            return response('subscription not found', 404);
        }

        $ps = PlanSubscription::where('user_id', $user->id)->whereRaw('ends_at < NOW()')->first();

        if(isset($ps->id)){
            return response('this user already subscribe a membership', 400);
        }

        if(isset($plan->is_addon) && !empty($plan->is_addon)){
            $pln = Plan::select('id')
            ->where(['tag'=>'corporate', 'is_addon'=>0])
            ->get();
            if($pln){
              $arrid = [];
              foreach($pln as $pl){
                array_push($arrid, $pl->id);
              }
              $csb = PlanSubscription::where(['user_id' => $user->id])
              ->whereDate('ends_at', '>=', date('Y-m-d'))
              ->whereIn('plan_id', $arrid)
              ->count();
              if(empty($csb)){
                return response('user must subscribe corporate first', 400);
              }
            }else{
                return response('no corporate plan existed', 404);
            }
        }

        $pldata = [
            'id'=>$plan->id,
            'tag'=>$plan->tag,
            'name'=>$plan->name,
            'price'=>$plan->price
        ];

        // if($user->pay($plan)) {
        //     $user->newSubscription('main', $plan, $plan->name, $plan->description);
        // }

        $fee = (!empty($request->input('fee'))) ? $request->input('fee') : 0;
        $trans = PaymentTransaction::create([
            'user_id' => $user->id,
            'payable_id' => $plan->id,
            'payable_type' => (is_object($plan)) ? get_class($plan) : "App\Model\Plan",
            'status'  => 0,
            'channel' => $request->input('channel'),
            'amount' => $plan->price,
            'fee' => $fee,
            'expire_in' => Carbon::now()->addMinutes(config('pga.duitku.expire_in')),
            'product' => $pldata
        ]);

        $format = str_pad((string) $trans->id, 6, '0', STR_PAD_LEFT);
        $invoice = "SB/{$trans->created_at->format('Ymd')}/{$format}";
        $trans->invoice_no = $invoice;
        $trans->save();

        $payment = (new Duitku)->create([
            'invoice_no' => $trans->invoice_no,
            'channel' => $trans->channel,
            'amount' => $trans->amount,
            'description' => 'Subscribption : '.$plan->tag.' '.$plan->name,
            'user' => [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phoneFormat,
                'address' => $user->address
            ],
            'items' => [
                [
                    'name' => $plan->tag.' '.$plan->name,
                    'price' => $plan->price,
                    'quantity' => 1
                ]
            ]
        ]);

        $trans->update(['payment_create' => $payment, 'reference_no' => $payment['reference']]);

        return response(new PaymentTransactionResource($trans));
    }

    #[RequestMapping(path: "/public/{usr}/plan/{pln}/subscribe/{channel}/{sales}/{fee}", methods: "get")]
    public function psub($usr, $pln, $channel, $sales, $fee, RequestInterface $request)
    {
        // $user = $this->auth->guard()->user();

        // if(! password_verify($request->input('password'), $user->password)) {
        //     return response('Invalid password.', ErrorCode::FORM_ERROR);
        // }

        $user = User::find($usr);
        if(!$user){
            return response('user not found', 404);
        }

        $plan = Plan::find($pln);
        if(!$plan){
            return response('subscription not found', 404);
        }

        $ps = PlanSubscription::where('user_id', $usr)->first();
        if(isset($ps->id)){
            return response('this user already subscribe membership', 400);
        }

        if(isset($plan->is_addon) && !empty($plan->is_addon)){
            $pln = Plan::select('id')
            ->where(['tag'=>'corporate', 'is_addon'=>0])
            ->get();
            if($pln){
              $arrid = [];
              foreach($pln as $pl){
                array_push($arrid, $pl->id);
              }
              $csb = PlanSubscription::where(['user_id' => $user->id])
              ->whereDate('ends_at', '>=', date('Y-m-d'))
              ->whereIn('plan_id', $arrid)
              ->count();
              if(empty($csb)){
                return response('user must subscribe corporate first', 400);
              }
            }else{
                return response('no corporate plan existed', 404);
            }
        }

        $pldata = [
            'id'=>$plan->id,
            'tag'=>$plan->tag,
            'name'=>$plan->name,
            'price'=>$plan->price
        ];

        if(!empty($sales)){
            $pldata["sales"] = $sales;
        }

        // if($user->pay($plan)) {
        //     $user->newSubscription('main', $plan, $plan->name, $plan->description);
        // }

        $fee = (isset($fee) && !empty($fee)) ? $fee : 0;
        $trans = PaymentTransaction::create([
            'user_id' => $user->id,
            'payable_id' => $plan->id,
            'payable_type' => (is_object($plan)) ? get_class($plan) : "App\Model\Plan",
            'status'  => 0,
            'channel' => $channel,
            'amount' => $plan->price,
            'fee' => $fee,
            'expire_in' => Carbon::now()->addMinutes(config('pga.duitku.expire_in')),
            'product' => $pldata
        ]);

        $format = str_pad((string) $trans->id, 6, '0', STR_PAD_LEFT);
        $invoice = "SB/{$trans->created_at->format('Ymd')}/{$format}";
        $trans->invoice_no = $invoice;
        $trans->save();

        if(!empty($channel) && $channel != "CASH"){
            $payment = (new Duitku)->create([
                'invoice_no' => $trans->invoice_no,
                'channel' => $trans->channel,
                'amount' => $trans->amount,
                'description' => 'Subscribption : '.$plan->tag.' '.$plan->name,
                'user' => [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'email' => $user->email,
                    'phone' => $user->phoneFormat,
                    'address' => $user->address
                ],
                'items' => [
                    [
                        'name' => $plan->tag.' '.$plan->name,
                        'price' => $plan->price,
                        'quantity' => 1
                    ]
                ]
            ]);

            $trans->update(['payment_create' => $payment, 'reference_no' => $payment['reference']]);
        }else{
            $trans->update(['status'=>1]);
            $this->event->dispatch(new PaymentSuccess($trans));
        }

        return response(new PaymentTransactionResource($trans));
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Payment;
use Carbon\Carbon;
use App\Payment\Duitku;
use App\Model\User;
use App\Model\CreditPackage;
use App\Model\PaymentTransaction;
use App\Model\Setting;
use App\Event\PaymentSuccess;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use App\Middleware\CallbackMiddleware;
use App\Resource\PaymentTransactionResource;
use App\Resource\CreditPackageResource;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

#[Controller]
class CreditController
{
    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected EventDispatcherInterface $event;

    #[RequestMapping(path: "/api/credit/packages", methods: "get")]
    public function index(RequestInterface $request)
    {
        $rpp = $request->input('per_page', 6);
        $rows = CreditPackage::paginate($rpp);

        return response(CreditPackageResource::collection($rows));
    }

    #[RequestMapping(path: "/api/credit/buy", methods: "post")]
    public function buy(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'package_id' => 'required|exists:credit_packages,id',
            'channel' => 'required'
        ])->validate();

        $package = CreditPackage::findOrFail($request->input('package_id'));
        $amount = $package->price_special > 0 ? $package->price_special: $package->price;

        if($package->discount > 0) {
            $amount = $package->price - (($package->price * $package->discount) / 100);
        }

        $fee = (!empty($request->input('fee'))) ? $request->input('fee') : 0;
        $trans = PaymentTransaction::create([
            'user_id' => $user->id,
            'payable_id' => $package->id,
            'payable_type' => get_class($package),
            'status'  => 0,
            'channel' => $request->input('channel'),
            'fee' => $fee,
            'amount' => $amount,
            'expire_in' => Carbon::now()->addMinutes(config('pga.duitku.expire_in')),
            'product' => $package->toArray()
        ]);

        $format = str_pad((string) $trans->id, 6, '0', STR_PAD_LEFT);
        $invoice = "CR/{$trans->created_at->format('Ymd')}/{$format}";
        $trans->invoice_no = $invoice;
        $trans->save();

        $payment = (new Duitku)->create([
            'invoice_no' => $trans->invoice_no,
            'channel' => $trans->channel,
            'amount' => $trans->amount,
            'description' => $package->name,
            'user' => [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phoneFormat,
                'address' => $user->address
            ],
            'items' => [
                [
                    'name' => $package->name,
                    'price' => $amount,
                    'quantity' => 1
                ]
            ]
        ]);

        $trans->update(['payment_create' => $payment, 'reference_no' => $payment['reference']]);

        return response(new PaymentTransactionResource($trans));
    }

    #[RequestMapping(path: "/public/{usr}/credit/{pkg}/buy/{channel}/{sales}/{fee}", methods: "get")]
    public function pbuy($usr, $pkg, $channel, $sales, $fee, RequestInterface $request)
    {
        $user = User::find($usr);
        if(!$user){
            return response('user not found', 404);
        }

        $package = CreditPackage::find($pkg);
        if(!isset($package)){
            return response('package credit not found', 404);
        }

        $amount = $package->price_special > 0 ? $package->price_special: $package->price;

        if($package->discount > 0) {
            $amount = $package->price - (($package->price * $package->discount) / 100);
        }

        $pkgarr = $package->toArray();
        if(!empty($sales)){
            $pkgarr['sales'] = $sales;
        }
        $trans = PaymentTransaction::create([
            'user_id' => $user->id,
            'payable_id' => $package->id,
            'payable_type' => get_class($package),
            'status'  => 0,
            'channel' => $channel,
            'fee' => $fee,
            'amount' => $amount,
            'expire_in' => Carbon::now()->addMinutes(config('pga.duitku.expire_in')),
            'product' => $pkgarr
        ]);

        $format = str_pad((string) $trans->id, 6, '0', STR_PAD_LEFT);
        $invoice = "CR/{$trans->created_at->format('Ymd')}/{$format}";
        $trans->invoice_no = $invoice;
        $trans->save();

        if(!empty($channel) && $channel != "CASH"){
            $payment = (new Duitku)->create([
                'invoice_no' => $trans->invoice_no,
                'channel' => $trans->channel,
                'amount' => $trans->amount,
                'description' => $package->name,
                'user' => [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'email' => $user->email,
                    'phone' => $user->phoneFormat,
                    'address' => $user->address
                ],
                'items' => [
                    [
                        'name' => $package->name,
                        'price' => $amount,
                        'quantity' => 1
                    ]
                ]
            ]);

            $trans->update(['payment_create' => $payment, 'reference_no' => $payment['reference']]);
        }else{
            $trans->update(['status'=>1]);
            $this->event->dispatch(new PaymentSuccess($trans));
        }

        return response(new PaymentTransactionResource($trans));
    }

    #[RequestMapping(path: "/api/payment/channels", methods: "get")]
    public function channels(RequestInterface $request)
    {
        $amount = (int) $request->input('amount', 10000);

        $channels = (new Duitku)->channels($amount);
        $setting = Setting::where(['key'=>'payment-howto'])->first();
        $jsonData = $chann = [];
        if(isset($setting->key)){
          $jsonData = (array)json_decode(file_get_contents($setting->value));
        }

        foreach($channels as $chn){
            $howto = [];
            if(isset($chn["paymentMethod"]) && isset($jsonData[$chn["paymentMethod"]])){
                $howto = $jsonData[$chn["paymentMethod"]];
            }
            $chn["howTo"]=$howto;
            $chann[] = $chn;
        }

        return response($chann);
    }

    #[RequestMapping(path: "/payment/callback", methods: "post")]
    #[Middleware(CallbackMiddleware::class)]
    public function callback(RequestInterface $request)
    {
        return (new Duitku)->callback($request->all());
    }

    #[RequestMapping(path: "/api/payment/check", methods: "post")]
    public function checkTopup(RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'invoice_no' => 'required'
        ])->validate();

        $invoice = PaymentTransaction::where('user_id', $user->id)
            ->where('invoice_no', $request->input('invoice_no'))
            ->first();
        if(!$invoice->invoice_no){
            return response("no invoice registered at this invoice number", 404);
        }
        $result = (new Duitku)->check($invoice->invoice_no);

        return response(new PaymentTransactionResource($result));
    }
}

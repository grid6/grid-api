<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\Tag;
use App\Model\Article;
use App\Model\Banner;
use App\Resource\TagResource;
use App\Resource\ArticleResource;
use App\Resource\BannerResource;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;

#[Controller]
class ArticleController
{
    #[RequestMapping(path: "/api/articles", methods: "get")]
    public function index(RequestInterface $request)
    {
        $rpp = $request->input('per_page', 6);

        $rows = Article::latest();

        if($request->has('tag')) {
            $rows = $rows->withAnyTags($request->input('tag'));
        }

        $rows = $rows->paginate($rpp);

        return response(ArticleResource::collection($rows));
    }

    #[RequestMapping(path: "/api/banners", methods: "get")]
    public function banners(RequestInterface $request)
    {
        $rpp = $request->input('per_page', 6);

        $rows = Banner::all();

        // $rows = $rows->paginate($rpp);

        return response(BannerResource::collection($rows));
    }

    #[RequestMapping(path: "/api/articles/{slug}", methods: "get")]
    public function show($slug, RequestInterface $request)
    {
        $row = Article::whereSlug($slug)->first();
        if(!$row->id){
            return response("data article not found", 404);
        }

        return response(new ArticleResource($row));
    }

    #[RequestMapping(path: "/api/tags", methods: "get")]
    public function tags()
    {
        $rows = Tag::all();
        return response(TagResource::collection($rows));
    }

    #[RequestMapping(path: "/api/tags/{tag}", methods: "get")]
    public function articleTag($tag, RequestInterface $request)
    {
        $rpp = $request->input('per_page', 6);

        $rows = Article::withAnyTags($tag)
            ->latest()
            ->paginate($rpp);

        return response(ArticleResource::collection($rows));
    }
}

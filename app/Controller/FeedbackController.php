<?php

declare(strict_types=1);

namespace App\Controller;

use Carbon\Carbon;
use Hyperf\DbConnection\Db;
use App\Model\Feedback;
use App\Model\FeedbackSubject;
use App\Model\FeedbackType;
use App\Constants\ErrorCode;
use App\Resource\FeedbackResource;
use App\Resource\FeedbackSubjectResource;
use App\Resource\FeedbackTypeResource;
use Hyperf\Di\Annotation\Inject;
use Qbhy\HyperfAuth\AuthManager;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;


#[Controller]
class FeedbackController
{
    #[Inject]
    protected ValidatorFactoryInterface $validator;

    #[Inject]
    protected AuthManager $auth;

    #[Inject]
    protected EventDispatcherInterface $event;

    #[RequestMapping(path: "/api/feedback/type", methods: "get")]
    public function types(RequestInterface $request)
    {
        $ftype = FeedbackType::where('status', 1)->get();

        if(!$ftype){
            return response('feedback type is empty', 0);
        }

        return response(FeedbackTypeResource::collection($ftype));
    }

    #[RequestMapping(path: "/api/feedback/type/{type}/subject", methods: "get")]
    public function subjects($type, RequestInterface $request)
    {

        $ftype = FeedbackType::find($type);
        if(!$ftype){
            return response('feedback type not found', 404);
        }

        $fsubj = FeedbackSubject::where(['feedback_type'=>$type, 'status'=>1])
        ->with('feedback_type')
        ->get();

        if(!$fsubj){
            return response('feedback subject is empty', 0);
        }

        return response(FeedbackSubjectResource::collection($fsubj));
    }

    #[RequestMapping(path: "/api/feedback/type/{type}/subject/{subjects}/send", methods: "post")]
    public function send($type, $subjects, RequestInterface $request)
    {
        $user = $this->auth->guard()->user();

        $this->validator->make($request->all(), [
            'subject' => 'required',
            'message' => 'required'
        ])->validate();

        $ftype = FeedbackType::find($type);
        if(!$ftype){
            return response('feedback type not found', 404);
        }

        $fsubj = FeedbackSubject::find($subjects);
        if(!$fsubj){
            return response('feedback subject not found', 404);
        }else if($fsubj->feedback_type != $type){
            return response('miss-match type for this subject', 400);
        }

        $fdback = Feedback::create([
            'user_id' => $user->id,
            'feedback_subject' => $subjects,
            'subject' => $request->input('subject'),
            'message' => $request->input('message')
        ]);

        return response(new FeedbackResource($fdback));
    }
}

<?php

namespace App\Xmqtt;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Hyperf\Utils\Codec\Json;
use App\Model\User;
use App\Model\Page;
use App\Model\PaymentTransaction;
use App\Model\TrainerLevelSession;
use App\Model\CreditPackage;
use Xtwoend\HySubscribe\Model\Plan;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Guzzle\CoroutineHandler;
use App\Pos\Exception\GridsException;
use Psr\EventDispatcher\EventDispatcherInterface;


class Mqtt
{
    const URL = 'https://rest-xmqtt.gridfitnesshub.com';
    const DEV_URL = 'https://dev-rest-xmqtt.gridfitnesshub.com';

    protected $http;

    #[Inject]
    protected EventDispatcherInterface $event;

    public function __construct() {
        $this->http = new Client([
            'base_uri' => config('mqtt.url', self::URL),
            'handler' => HandlerStack::create(new CoroutineHandler()),
            'timeout' => 15,
            'debug' => true,
            'swoole' => [
                'timeout' => 20,
                'socket_buffer_size' => 1024 * 1024 * 2,
            ],
        ]);
    }

    public function devices()
    {
        $result = $this->exec('GET', '/api/device');

        return (array) $result;
    }

    public function remove_batch(array $param = [])
    {
        $result = $this->exec('POST', '/api/remove-batch', $param);
        return $result;
    }

    public function enroll(array $data = [])
    {
        $image_parts = explode(";base64,", $data['face_registered']);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = (isset($image_type_aux[1])) ? $image_type_aux[1] : '';
        $image_base64 = (isset($image_parts[1])) ? base64_decode($image_parts[1]) : '';

        $params = [
                [
                    'name'     => 'devices',
                    'contents' => $data['device']
                ],
                [
                    'name'     => 'customer_id',
                    'contents' => $data['id']
                ],
                [
                    'name'     => 'name',
                    'contents' => $data['first_name'] . ' ' . $data['last_name']
                ],
                [
                    'name' => 'photo',
                    'contents' => $image_base64,
                    'filename' => 'registered-'.date("YmdHis").'.'.$image_type
                ],
        ];

        $resps = $this->exec_form('POST', '/api/enrollment', $params);
        return $resps;
    }

    private function exec($method, $path, $data = [], $token='')
    {
        $headers = [
            'headers' => [
              'Content-Type' => 'application/json',
              'Content-Length' =>  strlen(Json::encode($data))
            ],
            'json' => $data
        ];

        if(!empty($token)){
            $headers['headers'] = [
                'Content-Type' => 'application/json',
                'Content-Length' =>  strlen(Json::encode($data)),
                'Authorization' => 'Bearer '.$token
            ];
        }

        $result = $this->http->request($method, $path, $headers);
        $body = (string) $result->getBody();
        $json = Json::decode($body);
        $code = (int) $result->getStatusCode();

        if($code != 200) {
            throw new MqttException($json->Message, $code);
        }

        return $json;
    }

    private function exec_form($method, $path, $data = [], $token='', $multipart=true)
    {
        if(!$multipart){
          $headers = [
              'form_params' => $data
          ];
        }else{
          $headers = [
              'multipart' => $data
          ];
        }
        if(!empty($token)){
            $headers['headers'] = [
                'Authorization' => 'Bearer '.$token
            ];
        }

        $result = $this->http->request($method, $path, $headers);
        $body = (string) $result->getBody();
        $json = Json::decode($body);
        $code = (int) $result->getStatusCode();
        if($code != 200) {
            throw new MqttException($json->Message, $code);
        }

        return $json;
    }

    protected function array_keys_exists(array $keys, array $arr)
    {
        return !array_diff_key(array_flip($keys), $arr);
    }
}

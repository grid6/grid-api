<?php

namespace App\Xmqtt\Exception;

class MqttException extends \Exception
{
    protected $message;

    public function __construct(?string $message = null, int $code = 400) {
        parent::__construct($message, $code);
    }
}
